export default function ArrowUp() {
  return (
    <svg fill="none" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 30 29">
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M1.492 15.769c-.7-.701-.7-1.837 0-2.538L14.178.545c.7-.7 1.837-.7 2.537 0l12.687 12.686a1.794 1.794 0 1 1-2.538 2.538l-9.623-9.624v21.041a1.794 1.794 0 0 1-3.588 0V6.146l-9.624 9.623c-.7.7-1.837.7-2.537 0Z"
        fill="currentColor"
      />
    </svg>
  );
}
