import LinkedIn from '@/components/shared/icons/LinkedIn';
import Mastodon from '@/components/shared/icons/Mastodon';
import RSS from '@/components/shared/icons/RSS';
import classNames from 'classnames';
import { SocialLinksProps } from '.';
import styles from './SocialLinks.module.css';

export default function SocialLinks({ className }: SocialLinksProps) {
  return (
    <div className={classNames(styles.SocialLinks, className)}>
      <p className={classNames(styles.SocialLinks__title, 'is-visually-hidden')}>Social Links</p>

      <ul className={styles.SocialLinks__items} data-role="list">
        <li className={styles.SocialLinks__item}>
          <a
            className={styles.SocialLinks__link}
            href="https://howdee.social/@incrementbyone"
            rel="me"
            title="Follow us on Mastodon."
          >
            <Mastodon className={styles.SocialLinks__icon} />
            <span className="is-visually-hidden">Mastodon</span>
          </a>
        </li>
        <li className={styles.SocialLinks__item}>
          <a
            className={styles.SocialLinks__link}
            href="https://www.linkedin.com/company/increment-by-one/"
            rel="me"
            title="Follow us on LinkedIn."
          >
            <LinkedIn className={styles.SocialLinks__icon} />
            <span className="is-visually-hidden">LinkedIn</span>
          </a>
        </li>
        <li className={styles.SocialLinks__item}>
          <a
            className={styles.SocialLinks__link}
            href="/rss.xml"
            title="Subscribe to our news RSS feed."
          >
            <RSS className={styles.SocialLinks__icon} />
            <span className="is-visually-hidden">RSS</span>
          </a>
        </li>
      </ul>
    </div>
  );
}
