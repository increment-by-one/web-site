import { ClassName } from '@/lib/types';
import SocialLinks from './SocialLinks';

export default SocialLinks;

export type SocialLinksProps = {
  className?: ClassName;
};
