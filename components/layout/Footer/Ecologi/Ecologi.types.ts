import { ClassName } from '@/lib/types';

export type EcologiProps = {
  className?: ClassName;
};
