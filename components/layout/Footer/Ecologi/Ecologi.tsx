import classNames from 'classnames';
import Image from 'next/image';
import Link from 'next/link';
import styles from './Ecologi.module.css';
import { EcologiProps } from './Ecologi.types';

export default function Ecologi({ className }: EcologiProps) {
  return (
    <div className={classNames(styles.Ecologi, className)}>
      <div className={styles.Ecologi__image}>
        <Image src="/images/logos/ecologi.svg" alt="Ecologi" width="90" height="75" />
      </div>

      <div className={styles.Ecologi__inner}>
        <p>We&apos;ve partnered up with Ecologi to ensure a greener tomorrow.</p>
        <p>
          <Link href="/about#ecologi">
            <a>
              Learn More<span className="is-visually-hidden"> about our partnership.</span>
            </a>
          </Link>
        </p>
      </div>
    </div>
  );
}
