import EmailAddress from '@/components/shared/EmailAddress';
import Logo from '@/components/shared/Logo';
import Link from 'next/link';
import ArrowUp from './ArrowUp';
import Copyright from './Copyright';
import Ecologi from './Ecologi';
import styles from './Footer.module.css';
import SocialLinks from './SocialLinks';

export default function Footer() {
  return (
    <footer className={styles.Footer}>
      <div className={styles.Footer__row}>
        <p className={styles.Footer__tagline}>
          Taking you to the <span>next level</span>
        </p>

        <div className={styles.Links}>
          <nav className={styles.Navigation}>
            <ul className={styles.Navigation__items} data-role="list">
              <li className={styles.Navigation__item}>
                <Link href="/">
                  <a className={styles.Navigation__link}>Home</a>
                </Link>
              </li>
              <li className={styles.Navigation__item}>
                <Link href="/about">
                  <a className={styles.Navigation__link}>About</a>
                </Link>
              </li>
              <li className={styles.Navigation__item}>
                <Link href="/our-services">
                  <a className={styles.Navigation__link}>Services</a>
                </Link>
              </li>
              <li className={styles.Navigation__item}>
                <Link href="/our-work">
                  <a className={styles.Navigation__link}>Work</a>
                </Link>
              </li>
              <li className={styles.Navigation__item}>
                <Link href="/news">
                  <a className={styles.Navigation__link}>News</a>
                </Link>
              </li>
              <li className={styles.Navigation__item}>
                <Link href="/contact-us">
                  <a className={styles.Navigation__link}>Contact</a>
                </Link>
              </li>
            </ul>
          </nav>

          <SocialLinks className={styles.Footer__social} />
        </div>

        <Ecologi className={styles.Footer__ecologi} />
      </div>

      <hr className={styles.Footer__separator} />

      <div className={styles.Footer__row}>
        <div className={styles.Footer__item}>
          <Link href="/">
            <a>
              <Logo className={styles.Footer__logo} isInverted />
              <span className="is-visually-hidden">Increment By One</span>
            </a>
          </Link>
        </div>
        <div className={styles.Footer__item}>
          <EmailAddress className={styles.Footer__email} />
        </div>
        <div className={styles.Footer__item}>
          <Copyright />
        </div>
      </div>

      <a className={styles.Footer__toTop} href="#top">
        <ArrowUp />
        <span className="is-visually-hidden">Back to top</span>
      </a>
    </footer>
  );
}
