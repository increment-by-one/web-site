import styles from './Copyright.module.css';

export default function Copyright() {
  return (
    <p className={styles.Copyright}>
      <small>Copyright &copy; 2016 - {new Date().getFullYear()} Increment By One Ltd.</small>
      <br />
      <small>Registered in England &amp; Wales, company number 9656807.</small>
      <br />
      <small>VAT registered with number 291472783.</small>
    </p>
  );
}
