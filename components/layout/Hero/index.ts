import Hero from './Hero';

export * from './Hero.types';
export { default as Inner } from './Inner';
export { default as HomeInner } from './HomeInner';

export default Hero;
