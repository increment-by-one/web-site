import Badge from '@/components/shared/Badge';
import classNames from 'classnames';
import Image from 'next/image';
import Header from '../Header';
import styles from './Hero.module.css';
import { HeroProps, HeroSize, HeroVariant } from './Hero.types';

export default function Hero({
  background,
  badge,
  children,
  size = HeroSize.DEFAULT,
  title,
  variant,
}: HeroProps) {
  const isInverted =
    variant && [HeroVariant.INVERTED, HeroVariant.INVERTED_SQUARES].includes(variant);
  const showInner = Boolean(title || children);

  const IntroElement = title ? 'section' : 'div';

  return (
    <div
      className={classNames(styles.Hero, {
        [styles['Hero--inverted']]: variant === HeroVariant.INVERTED,
        [styles['Hero--inverted-squares']]: variant === HeroVariant.INVERTED_SQUARES,
        [styles['Hero--with-background']]: Boolean(background),
        [styles[`Hero--size-${size}`]]: size !== HeroSize.DEFAULT,
      })}
    >
      <Header className={styles.Hero__header} isInverted={isInverted} />

      {showInner && (
        <IntroElement className={styles.Intro}>
          {title && (
            <header className={styles.Intro__header}>
              {badge && <Badge>{badge}</Badge>}
              <h1 className={styles.Intro__title}>{title}</h1>
            </header>
          )}

          {children && <div className={styles.Intro__content}>{children}</div>}
        </IntroElement>
      )}

      {background && (
        <div className={styles.Hero__background}>
          <Image
            priority
            src={background}
            layout="fill"
            alt=""
            objectFit="cover"
            objectPosition="center"
          />
        </div>
      )}
    </div>
  );
}
