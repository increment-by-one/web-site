import { PropsWithChildren } from 'react';

export type HeroProps = PropsWithChildren<{
  background?: string;
  badge?: string;
  size?: HeroSize;
  title?: string;
  variant?: HeroVariant;
}>;

export type InnerProps = PropsWithChildren<{
  as?: InnerElement;
}>;

export enum InnerElement {
  DIV = 'div',
  SECTION = 'section',
}

export enum HeroSize {
  DEFAULT = 'default',
  LARGE = 'large',
}

export enum HeroVariant {
  INVERTED = 'inverted',
  INVERTED_SQUARES = 'inverted-squares',
  LETTERBOX = 'letterbox',
  WITH_IMAGE = 'with-image',
}
