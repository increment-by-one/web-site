import Availability from '@/components/home/Availability';
import Button, { ButtonSize } from '@/components/shared/Button';
import styles from './HomeInner.module.css';

export default function HomeInner() {
  return (
    <div className={styles.HomeInner}>
      <h1 className={styles.HomeInner__title}>Meet your new outsourced development team</h1>

      <div className={styles.HomeInner__content}>
        <p>
          We take your software development challenges and deliver incremental change that drives
          commercial success.
        </p>
      </div>

      <footer className={styles.HomeInner__footer}>
        <Button href="/contact-us" size={ButtonSize.LARGE}>
          Get in touch now
        </Button>

        <Availability
          className={styles.HomeInner__availability}
          isAvailableAt={new Date(2024, 10, 1)}
        />
      </footer>
    </div>
  );
}
