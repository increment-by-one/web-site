import styles from './Hero.module.css';
import { InnerElement, InnerProps } from './Hero.types';

export default function Inner({ as: Element = InnerElement.DIV, children }: InnerProps) {
  return <Element className={styles.Hero__inner}>{children}</Element>;
}
