import Link from 'next/link';
import Logo from '@/components/shared/Logo';
import VisuallyHidden from '@/components/shared/VisuallyHidden';
import styles from './Header.module.css';
import { HeaderProps } from './Header.types';
import Button, { ButtonLevel } from '@/components/shared/Button';
import classNames from 'classnames';
import EmailAddress from '@/components/shared/EmailAddress';
import BurgerIcon from '@/components/shared/icons/Burger';
import XIcon from '@/components/shared/icons/X';

export default function Header({ className, isInverted = false }: HeaderProps) {
  return (
    <header
      className={classNames(styles.Header, { [styles['Header--inverted']]: isInverted }, className)}
      id="top"
    >
      <Link href="/">
        <a className={styles.Header__Logo}>
          <Logo isInverted={isInverted} />
          <VisuallyHidden>Increment By One</VisuallyHidden>
        </a>
      </Link>

      <div>
        <label className={styles.Header__navTriggerLabel} htmlFor="_nav-primary" aria-hidden>
          <BurgerIcon />
        </label>
        <input
          className={styles.Header__navTriggerInput}
          type="checkbox"
          id="_nav-primary"
          aria-hidden
        />
        <div className={styles.OffCanvas}>
          <div className={styles.OffCanvas__contact}>
            <Link href="/">
              <a>
                <Logo className={styles.OffCanvas__logo} aria-hidden />
                <span className="is-visually-hidden">Increment By One</span>
              </a>
            </Link>
            <EmailAddress className={styles.OffCanvas__email} />
          </div>

          <label className={styles.Header__navTriggerLabel} htmlFor="_nav-primary" aria-hidden>
            <XIcon />
          </label>

          <nav className={styles.Navigation}>
            <ul className={styles.Navigation__items} data-role="list">
              <li className={styles.Navigation__item}>
                <Link href="/about">
                  <a className={styles.Navigation__link}>About</a>
                </Link>
              </li>
              <li className={styles.Navigation__item}>
                <Link href="/our-services">
                  <a className={styles.Navigation__link}>Our Services</a>
                </Link>

                <ul className={styles.Navigation__items} data-role="list">
                  <li className={styles.Navigation__item}>
                    <Link href="/our-services/web-application-development">
                      <a className={styles.Navigation__link}>Web Application Development</a>
                    </Link>
                  </li>
                  <li className={styles.Navigation__item}>
                    <Link href="/our-services/server-side-development">
                      <a className={styles.Navigation__link}>Server-side Development</a>
                    </Link>
                  </li>
                  <li className={styles.Navigation__item}>
                    <Link href="/our-services/development-consultancy">
                      <a className={styles.Navigation__link}>Development Consultancy</a>
                    </Link>
                  </li>
                </ul>
              </li>
              <li className={styles.Navigation__item}>
                <Link href="/our-work">
                  <a className={styles.Navigation__link}>Our Work</a>
                </Link>
              </li>
              <li className={styles.Navigation__item}>
                <Link href="/news">
                  <a className={styles.Navigation__link}>News</a>
                </Link>
              </li>
              <li className={styles.Navigation__item}>
                <Button
                  href="/contact-us"
                  className={styles.Navigation__linkBtn}
                  level={isInverted ? ButtonLevel.SECONDARY : ButtonLevel.PRIMARY}
                >
                  Get In Touch
                </Button>
              </li>
            </ul>
          </nav>
        </div>
      </div>
    </header>
  );
}
