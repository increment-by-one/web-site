import { ClassName } from '@/lib/types';
import React from 'react';

export type HeaderProps = React.PropsWithChildren<{
  className?: ClassName;
  isInverted?: boolean;
}>;
