import { PropsWithChildren } from 'react';
import styles from './PageSection.module.css';

export default function Inner({ children }: PropsWithChildren<{}>) {
  return <div className={styles.PageSection__Inner}>{children}</div>;
}
