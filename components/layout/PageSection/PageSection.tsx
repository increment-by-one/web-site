import {
  PageSectionElement,
  PageSectionHeaderElement,
  PageSectionHeaderSize,
  PageSectionProps,
  PageSectionType,
  TitleLevel,
} from './PageSection.types';
import styles from './PageSection.module.css';
import classNames from 'classnames';
import Inner from './Inner';
import Header from './Header';
import Image from 'next/image';

export default function PageSection({
  as: Element = PageSectionElement.SECTION,
  children,
  className,
  id,
  backgroundImageUrl = '',
  badge = '',
  title = '',
  titleLevel = TitleLevel.H1,
  titleSize = PageSectionHeaderSize.DEFAULT,
  variant = PageSectionType.DEFAULT,
}: PageSectionProps) {
  return (
    <Element
      className={classNames(
        styles.PageSection,
        {
          [styles[`PageSection--${variant}`]]: variant !== PageSectionType.DEFAULT,
        },
        className
      )}
      {...(id && { id })}
    >
      <div className={styles.PageSection__content}>
        {title && (
          <Header
            as={
              Element === PageSectionElement.SECTION
                ? PageSectionHeaderElement.HEADER
                : PageSectionHeaderElement.DIV
            }
            badge={badge}
            titleLevel={titleLevel}
            size={titleSize}
          >
            {title}
          </Header>
        )}
        <Inner>{children}</Inner>
      </div>
      {backgroundImageUrl && (
        <aside className={styles.PageSection__background}>
          <Image src={backgroundImageUrl} layout="fill" alt="" />
        </aside>
      )}
    </Element>
  );
}
