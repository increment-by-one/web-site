import classNames from 'classnames';
import Badge from '@/components/shared/Badge';
import styles from './PageSection.module.css';
import {
  HeaderProps,
  PageSectionHeaderAlignment,
  PageSectionHeaderElement,
  PageSectionHeaderSize,
  PageSectionTitleStyle,
  TitleLevel,
} from './PageSection.types';

export default function Header({
  aligned = PageSectionHeaderAlignment.DEFAULT,
  as: Element = PageSectionHeaderElement.HEADER,
  badge,
  children,
  size,
  titleLevel: TitleElement = TitleLevel.H1,
  titleStyle,
}: HeaderProps) {
  return (
    <Element
      className={classNames(styles.PageSectionHeader, {
        [styles[`PageSectionHeader--aligned-${aligned}`]]:
          aligned !== PageSectionHeaderAlignment.DEFAULT,
        [styles[`PageSectionHeader--${size}`]]: size !== PageSectionHeaderSize.DEFAULT,
        [styles[`PageSectionHeader--${titleStyle}`]]: titleStyle !== PageSectionTitleStyle.DEFAULT,
      })}
    >
      <TitleElement className={styles.PageSection__title}>
        {badge && <Badge>{badge}</Badge>}
        {children}
      </TitleElement>
    </Element>
  );
}
