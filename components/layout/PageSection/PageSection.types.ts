import { ClassName } from '@/lib/types';
import { PropsWithChildren } from 'react';

export type PageSectionProps = PropsWithChildren<{
  as?: PageSectionElement;
  className?: ClassName;
  badge?: string;
  backgroundImageUrl?: string;
  titleLevel?: TitleLevel;
  id?: string;
  title?: string;
  titleSize?: PageSectionHeaderSize;
  variant?: PageSectionType;
}>;

export enum PageSectionElement {
  SECTION = 'section',
  DIV = 'div',
}

export enum PageSectionHeaderElement {
  HEADER = 'header',
  DIV = 'div',
}

export enum TitleLevel {
  H1 = 'h1',
  H2 = 'h2',
  H3 = 'h3',
  H4 = 'h4',
  H5 = 'h5',
  H6 = 'h6',
}

export enum PageSectionType {
  DEFAULT = 'default',
  HIGHLIGHT = 'highlight',
  INVERTED_LIGHT = 'inverted-light',
  INVERTED_DARK = 'inverted-dark',
  SPLIT_REVERSED = 'split-reversed',
  SPLIT = 'split',
  SUMMARY = 'summary',
}

export enum PageSectionHeaderAlignment {
  DEFAULT = 'default',
  LEFT = 'left',
}

export enum PageSectionHeaderSize {
  DEFAULT = 'default',
  SMALL = 'small',
  MEDIUM = 'medium',
  LARGE = 'large',
}

export enum PageSectionTitleStyle {
  DEFAULT = 'default',
  GRADIENT = 'gradient',
}

export type HeaderProps = PropsWithChildren<{
  aligned?: PageSectionHeaderAlignment;
  as?: PageSectionHeaderElement;
  badge?: string;
  size?: PageSectionHeaderSize;
  titleLevel?: TitleLevel;
  titleStyle?: PageSectionTitleStyle;
}>;
