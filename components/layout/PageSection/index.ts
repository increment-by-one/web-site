import PageSection from './PageSection';

export * from './PageSection.types';

export { default as Header } from './Header';
export { default as Inner } from './Inner';

export default PageSection;
