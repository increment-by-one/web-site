import Image from 'next/image';
import Link from 'next/link';
import styles from './PostItem.module.css';
import ArrowIcon from '@/components/shared/icons/Arrow';
import { PostItemProps } from '@/components/content/PostItem/PostItem.types';
import Author from '@/components/content/Author';
import format from 'date-fns/format';

const Post = ({ author, permalink, summary, title, images, publishedAt }: PostItemProps) => {
  const publishedAtTime = new Date(publishedAt);

  return (
    <article className={styles.PostItem}>
      <Link href={permalink}>
        <a>
          <figure className={styles.PostItem__image}>
            <Image src={images.summary} alt="" layout="fill" />
          </figure>
          <div className={styles.PostItem__inner}>
            <div className={styles.PostItem__content}>
              <header>
                <h2 className={styles.PostItem__title}>{title}</h2>
              </header>
              <p className={styles.PostItem__excerpt}>{summary}</p>
            </div>
            <footer>
              <div className={styles.PostItem__credits}>
                <Author {...author}>
                  <time dateTime={publishedAtTime.toISOString()}>
                    {format(publishedAtTime, 'MMM do, yyyy')}
                  </time>
                </Author>
              </div>

              <span className={styles.PostItem__permalink}>
                <ArrowIcon className={styles.PostItem__arrow} />
              </span>
            </footer>
          </div>
        </a>
      </Link>
    </article>
  );
};

export default Post;
