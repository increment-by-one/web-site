import { ClassName } from '@/lib/types';
import classNames from 'classnames';
import Image from 'next/image';

export default function CaseStudyLogo({
  alt,
  className,
  height,
  src,
  width,
}: {
  alt: string;
  className?: ClassName;
  height: number;
  src: string;
  width: number;
}) {
  return (
    <span className={classNames(className)}>
      <Image {...{ alt, height, src, width, layout: 'fixed' }} />
    </span>
  );
}
