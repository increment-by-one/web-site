import { CaseStudyGridProps } from './CaseStudyGrid.types';
import CaseStudyItem from './CaseStudyItem';
import styles from './CaseStudyGrid.module.css';

export default function CaseStudyGrid({ posts }: CaseStudyGridProps) {
  return (
    <div className={styles.CaseStudyGrid}>
      {posts.map((post) => (
        <CaseStudyItem key={post.slug} {...post} />
      ))}
    </div>
  );
}
