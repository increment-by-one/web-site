import BorderedImage from '@/components/shared/BorderedImage';
import Button, { ButtonLevel } from '@/components/shared/Button';
import Image from 'next/image';
import Link from 'next/link';
import { CaseStudyItemProps } from './CaseStudyGrid.types';
import styles from './CaseStudyItem.module.css';
import CaseStudyLogo from './CaseStudyLogo';

export default function CaseStudyItem({
  images,
  logo,
  permalink,
  summary,
  title,
}: CaseStudyItemProps) {
  return (
    <article className={styles.CaseStudyItem}>
      <div className={styles.CaseStudyItem__inner}>
        <header className={styles.CaseStudyItem__header}>
          <h1 className={styles.CaseStudyItem__title}>
            {logo ? (
              <CaseStudyLogo {...{ ...logo, className: styles.CaseStudyItem__logo, alt: title }} />
            ) : (
              title
            )}
          </h1>
        </header>

        <figure className={styles.CaseStudyItem__image}>
          <Link href={permalink}>
            <a className={styles.CaseStudyItem__link}>
              <Image src={images.summary} layout="fill" alt="" />
              <span className="is-visually-hidden">View case study.</span>
            </a>
          </Link>
        </figure>

        <div className={styles.CaseStudyItem__summary}>
          <p>{summary}</p>
        </div>

        <footer className={styles.CaseStudyItem__footer}>
          <Button href={permalink} level={ButtonLevel.SECONDARY}>
            View Case Study
          </Button>
        </footer>
      </div>
    </article>
  );
}
