import { Post } from '@/lib/types';

export type CaseStudyGridProps = {
  posts: Post[];
};

export type CaseStudyItemProps = Post;
