import PostItem from '../PostItem';
import { PostGridProps } from './PostGrid.types';
import styles from './PostGrid.module.css';

export default function PostGrid({ posts }: PostGridProps) {
  return (
    <div className={styles.PostGrid}>
      {posts.map((post) => (
        <PostItem key={post.slug} {...post} />
      ))}
    </div>
  );
}
