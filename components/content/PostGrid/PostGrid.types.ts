import { Post } from '@/lib/types';

export type PostGridProps = {
  posts: Post[];
};
