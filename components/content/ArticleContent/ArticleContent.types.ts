import { ClassName } from '@/lib/types';

export type ArticleContentProps = {
  className?: ClassName;
  content: string;
};
