import { ArticleContentProps } from './ArticleContent.types';
import styles from './ArticleContent.module.css';
import classNames from 'classnames';

export default function ArticleContent({ className, content }: ArticleContentProps) {
  return (
    <div
      className={classNames(styles.ArticleContent, className)}
      dangerouslySetInnerHTML={{ __html: content }}
    />
  );
}
