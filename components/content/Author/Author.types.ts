import { ClassName, PostAuthor } from '@/lib/types';
import { PropsWithChildren } from 'react';

export type AuthorProps = PropsWithChildren<
  PostAuthor & {
    className?: ClassName;
    isInverted?: boolean;
  }
>;
