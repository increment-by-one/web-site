import styles from './Author.module.css';
import Image from 'next/image';
import { AuthorProps } from './Author.types';
import classNames from 'classnames';

export default function Author({
  avatar,
  children,
  className,
  isInverted = false,
  name,
}: AuthorProps) {
  return (
    <div
      className={classNames(styles.Author, { [styles['Author--inverted']]: isInverted }, className)}
    >
      <figure className={styles.Author__avatar}>
        <Image src={avatar} alt="" layout="fill" />
      </figure>
      <div className={styles.Author__content}>
        <p className={styles.Author__name}>{name}</p>
        {children && <div className={styles.Author__text}>{children}</div>}
      </div>
    </div>
  );
}
