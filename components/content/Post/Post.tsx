import { PostProps } from './Post.types';
import styles from './Post.module.css';
import ArticleContent from '../ArticleContent';

export default function Post({ content }: PostProps) {
  return (
    <article className={styles.Post}>
      <ArticleContent content={content} className={styles.Post__content} />
    </article>
  );
}
