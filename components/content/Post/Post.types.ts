import { Post } from '@/lib/types';

export type PostProps = Post;

export type PostContentProps = { content: string };
