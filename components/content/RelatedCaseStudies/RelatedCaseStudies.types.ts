import { Post } from '@/lib/types';

export type RelatedCaseStudiesProps = {
  posts: Post[];
};
