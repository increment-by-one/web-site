import BorderedImage from '@/components/shared/BorderedImage';
import { Post } from '@/lib/types';
import Image from 'next/image';
import styles from './RelatedCaseStudy.module.css';

export default function RelatedCaseStudy({ images, title }: Post) {
  return (
    <article className={styles.RelatedCaseStudy}>
      <h1 className={styles.RelatedCaseStudy__title}>{title}</h1>
      <div className={styles.RelatedCaseStudy__image}>
        <Image src={images.summary} layout="fill" alt="" />
      </div>
    </article>
  );
}
