import RelatedCaseStudies from './RelatedCaseStudies';

export * from './RelatedCaseStudies.types';

export default RelatedCaseStudies;
