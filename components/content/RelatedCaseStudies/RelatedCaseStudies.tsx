import { RelatedCaseStudiesProps } from './RelatedCaseStudies.types';
import styles from './RelatedCaseStudies.module.css';
import RelatedCaseStudy from './RelatedCaseStudy';
import Link from 'next/link';

export default function RelatedCaseStudies({ posts }: RelatedCaseStudiesProps) {
  return (
    <div className={styles.RelatedCaseStudies}>
      {posts.map((post) => (
        <div key={post.slug} className={styles.RelatedCaseStudies__item}>
          <Link key={post.slug} href={post.permalink}>
            <a>
              <RelatedCaseStudy {...post} />
            </a>
          </Link>
        </div>
      ))}
    </div>
  );
}
