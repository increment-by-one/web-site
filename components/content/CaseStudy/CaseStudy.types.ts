import { ClassName, ImageSrcWithAlt, Post, PostIntro } from '@/lib/types';

export type CaseStudyProps = Post;

export type ImageGridProps = {
  className?: ClassName;
  images: ImageSrcWithAlt[];
};

export type IntroProps = PostIntro & {
  className?: ClassName;
};
