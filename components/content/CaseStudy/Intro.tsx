import PageSection, { PageSectionElement, PageSectionType } from '@/components/layout/PageSection';
import { IntroProps } from './CaseStudy.types';

export default function Intro({ className, short, summary }: IntroProps) {
  return (
    <PageSection
      as={PageSectionElement.DIV}
      className={className}
      variant={PageSectionType.SUMMARY}
      title={short}
    >
      <>
        {!Array.isArray(summary) ? (
          <p>{summary}</p>
        ) : (
          summary.map((paragraph, i) => <p key={i}>{paragraph}</p>)
        )}
      </>
    </PageSection>
  );
}
