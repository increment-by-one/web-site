import ArticleContent from '../ArticleContent';
import { CaseStudyProps } from './CaseStudy.types';
import ImageGrid from './ImageGrid';
import Intro from './Intro';
import styles from './CaseStudy.module.css';
import VisuallyHidden from '@/components/shared/VisuallyHidden';

export default function CaseStudy({ content, images, intro, title }: CaseStudyProps) {
  return (
    <article className={styles.CaseStudy}>
      <header>
        <h2>
          <VisuallyHidden>{title}</VisuallyHidden>
        </h2>
      </header>

      <Intro {...{ ...intro, className: styles.CaseStudy__intro }} />

      <ImageGrid images={images?.article || []} className={styles.CaseStudy__images} />

      <ArticleContent content={content} className={styles.CaseStudy__content} />
    </article>
  );
}
