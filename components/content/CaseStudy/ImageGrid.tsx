import classNames from 'classnames';
import Image from 'next/image';
import { ImageGridProps } from './CaseStudy.types';
import styles from './ImageGrid.module.css';

export default function ImageGrid({ className, images }: ImageGridProps) {
  return (
    <div className={classNames(styles.ImageGrid, className)}>
      {images.map(({ src, alt = '' }, i) => (
        <div key={i} className={styles.ImageGrid__image}>
          <Image src={src} alt={alt} layout="fill" />
        </div>
      ))}
    </div>
  );
}
