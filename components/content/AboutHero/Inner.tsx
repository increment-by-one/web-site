import { PropsWithChildren } from 'react';
import styles from './Inner.module.css';

export default function Inner({ children }: PropsWithChildren<{}>) {
  return <div className={styles.Inner}>{children}</div>;
}
