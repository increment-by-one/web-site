import RelatedPosts from './RelatedPosts';

export * from './RelatedPosts.types';

export default RelatedPosts;
