import { Post } from '@/lib/types';

export type RelatedPostsProps = {
  posts: Post[];
};
