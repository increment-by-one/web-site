import PostGrid from '../PostGrid';
import { RelatedPostsProps } from './RelatedPosts.types';
import PageSection, {
  PageSectionHeaderSize,
  PageSectionType,
} from '@/components/layout/PageSection';

export default function RelatedPosts({ posts }: RelatedPostsProps) {
  return (
    <PageSection
      variant={PageSectionType.HIGHLIGHT}
      title="Other posts you may like"
      titleSize={PageSectionHeaderSize.SMALL}
    >
      <PostGrid posts={posts} />
    </PageSection>
  );
}
