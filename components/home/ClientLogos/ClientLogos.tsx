import Image from 'next/image';
import styles from './ClientLogos.module.css';

export default function ClientLogos() {
  return (
    <aside className={styles.ClientLogos}>
      <ul className={styles.ClientLogos__list} data-role="list">
        <li className={styles.ClientLogos__listItem}>
          <Image
            className={styles.ClientLogos__logo}
            src="/images/our-work/clients/teamhouse.svg"
            height={37}
            width={179}
            alt="Teamhouse."
            layout="fixed"
          />
        </li>
        <li className={styles.ClientLogos__listItem}>
          <Image
            className={styles.ClientLogos__logo}
            src="/images/our-work/clients/haystack.svg"
            height={41}
            width={144}
            alt="Haystack."
            layout="fixed"
          />
        </li>
        <li className={styles.ClientLogos__listItem}>
          <Image
            className={styles.ClientLogos__logo}
            src="/images/our-work/clients/successful-media.svg"
            height={48}
            width={136}
            alt="Successful Media."
            layout="fixed"
          />
        </li>
        <li className={styles.ClientLogos__listItem}>
          <Image
            className={styles.ClientLogos__logo}
            src="/images/our-work/clients/salesfire.svg"
            height={45}
            width={104}
            alt="Salesfire."
            layout="fixed"
          />
        </li>
        <li className={styles.ClientLogos__listItem}>
          <Image
            className={styles.ClientLogos__logo}
            src="/images/our-work/clients/tailhail.svg"
            height={70}
            width={120}
            alt="Tailhail."
            layout="fixed"
          />
        </li>
        <li className={styles.ClientLogos__listItem}>
          <Image
            className={styles.ClientLogos__logo}
            src="/images/our-work/clients/raised-by.png"
            height={54}
            width={158}
            alt="Raised By."
            layout="fixed"
          />
        </li>
        <li className={styles.ClientLogos__listItem}>
          <Image
            className={styles.ClientLogos__logo}
            src="/images/our-work/clients/durham-university.svg"
            height={66}
            width={161}
            alt="Durham University."
            layout="fixed"
          />
        </li>
        <li className={styles.ClientLogos__listItem}>
          <Image
            className={styles.ClientLogos__logo}
            src="/images/our-work/clients/teconnex.png"
            height={57}
            width={153}
            alt="Teconnex."
            layout="fixed"
          />
        </li>
        <li className={styles.ClientLogos__listItem}>
          <Image
            className={styles.ClientLogos__logo}
            src="/images/our-work/clients/teamland.svg"
            height={47}
            width={150}
            alt="Teamland."
            layout="fixed"
          />
        </li>
        <li className={styles.ClientLogos__listItem}>
          <Image
            className={styles.ClientLogos__logo}
            src="/images/our-work/clients/ride-electric.svg"
            height={40}
            width={158}
            alt="Ride Electric."
            layout="fixed"
          />
        </li>
        <li className={styles.ClientLogos__listItem}>
          <Image
            className={styles.ClientLogos__logo}
            src="/images/our-work/clients/equitas.svg"
            height={37}
            width={144}
            alt="Equitas."
            layout="fixed"
          />
        </li>
      </ul>
    </aside>
  );
}
