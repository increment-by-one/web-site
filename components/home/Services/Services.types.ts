import { PropsWithChildren, ReactNode } from 'react';

export type ServiceProps = PropsWithChildren<{
  illustration: string;
  title: string;
  href: string;
  cta: string;
}>;
