import Link from 'next/link';
import Service from './Service';
import styles from './Services.module.css';

export default function Services() {
  return (
    <div className={styles.Services}>
      <Service
        title="Web Application Development"
        href="/our-services/web-application-development"
        illustration="web-application-development"
        cta="Learn more about web app development"
      >
        <p>
          Building a successful web application means combining a well-designed front-end user
          interface with a robust, secure database.
        </p>
        <p>
          We’ll take all of your requirements into account and build a web application that works
          for your business, and most importantly, your end-users.
        </p>
      </Service>

      <Service
        title="Server-Side Development"
        href="/our-services/server-side-development"
        illustration="server-side-development"
        cta="Visit server side development"
      >
        <p>
          The &apos;unseen&apos; side of development can be misunderstood or underestimated, leading
          to neglect and often breakdown. We can help you rescue existing systems or build new
          robust backend servers to power your business or product.
        </p>
      </Service>

      <Service
        title="Development Consultancy"
        href="/our-services/development-consultancy"
        illustration="development-consultancy"
        cta="See development consultancy services"
      >
        <p>
          Lots of businesses know they need better technology or systems, but have no idea how to
          get there. Don&apos;t worry - it&apos;s our job to help guide your development project
          within the context of your wider business. We&apos;ll ask questions such as: how can it
          improve the experience of your staff and customers? How can it help you to grow?
          Let&apos;s work together to find out.
        </p>
      </Service>
    </div>
  );
}
