import Image from 'next/image';
import Link from 'next/link';
import styles from './Service.module.css';
import { ServiceProps } from './Services.types';

export default function Service({ children, cta, href, illustration, title }: ServiceProps) {
  return (
    <article className={styles.Service}>
      <header>
        <aside className={styles.Service__icon}>
          <Image
            src={`/images/illustrations/${illustration}.svg`}
            width={100}
            height={100}
            alt=""
          />
        </aside>
        <h2 className={styles.Service__title}>{title}</h2>
      </header>

      {children && <div className={styles.Service__content}>{children}</div>}

      <footer className={styles.Service__footer}>
        <Link href={href}>
          <a>
            {cta} <span className="is-visually-hidden">about {title.toLocaleLowerCase()}</span>
          </a>
        </Link>
      </footer>
    </article>
  );
}
