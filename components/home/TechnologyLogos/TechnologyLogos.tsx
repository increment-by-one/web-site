import {
  Laravel as LaravelLogo,
  NodeJS as NodeJSLogo,
  PHP as PHPLogo,
  ReactJS as ReactJSLogo,
  VueJS as VueJSLogo,
} from '@/components/shared/logos';
import styles from './TechnologyLogos.module.css';

export default function TechnologyLogos() {
  return (
    <ul className={styles.TechnologyLogos} data-role="list">
      <li className={styles.TechnologyLogo__item}>
        <NodeJSLogo />
      </li>
      <li className={styles.TechnologyLogo__item}>
        <LaravelLogo />
      </li>
      <li className={styles.TechnologyLogo__item}>
        <PHPLogo />
      </li>
      <li className={styles.TechnologyLogo__item}>
        <ReactJSLogo />
      </li>
      <li className={styles.TechnologyLogo__item}>
        <VueJSLogo />
      </li>
    </ul>
  );
}
