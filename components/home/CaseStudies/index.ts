import CaseStudies from './CaseStudies';

export * from './CaseStudies.types';

export { default as CaseStudy } from './CaseStudy';

export default CaseStudies;
