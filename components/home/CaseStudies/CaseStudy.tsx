import BorderedImage, { AllowedImageElement } from '@/components/shared/BorderedImage';
import classNames from 'classnames';
import { CaseStudyProps } from './CaseStudies.types';
import styles from './CaseStudy.module.css';
import CaseStudyLogo from '@/components/content/CaseStudyGrid/CaseStudyLogo';
import Button, { ButtonLevel } from '@/components/shared/Button';
import Image from 'next/image';

export default function CaseStudy({
  images,
  permalink,
  summary,
  title,
  logo,
  isReversed = false,
}: CaseStudyProps) {
  return (
    <article
      className={classNames(styles.CaseStudy, { [styles['CaseStudy--reversed']]: isReversed })}
    >
      <div className={styles.CaseStudy__inner}>
        <header className={styles.CaseStudy__header}>
          <h2>
            {logo ? (
              <CaseStudyLogo {...{ ...logo, className: styles.CaseStudyItem__logo, alt: title }} />
            ) : (
              title
            )}
          </h2>
        </header>

        <div className={styles.CaseStudy__summary}>
          <p>{summary}</p>
        </div>

        <footer className={styles.CaseStudy__footer}>
          <Button href={permalink} level={ButtonLevel.SECONDARY}>
            View Case Study
          </Button>
        </footer>
      </div>

      <aside className={styles.CaseStudy__image}>
        <Image src={images.summary} layout="fill" alt="" />
      </aside>
    </article>
  );
}
