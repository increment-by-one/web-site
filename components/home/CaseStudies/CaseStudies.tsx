import styles from './CaseStudies.module.css';
import { CaseStudiesProps } from './CaseStudies.types';
import CaseStudy from './CaseStudy';

export default function CaseStudies({ caseStudies = [] }: CaseStudiesProps) {
  return (
    <div className={styles.CaseStudies}>
      {caseStudies.map((caseStudy, i) => (
        <CaseStudy key={caseStudy.slug} {...{ ...caseStudy, isReversed: i % 2 !== 0 }} />
      ))}
    </div>
  );
}
