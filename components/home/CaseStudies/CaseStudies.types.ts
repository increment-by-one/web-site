import { Post } from '@/lib/types';

export type CaseStudiesProps = {
  caseStudies: Post[];
};

export type CaseStudyProps = Post & { isReversed?: boolean };

export type CaseStudySummary = {
  title: string;
  image: string;
  slug: string;
  content: string;
  summary: string;
  publishedAt?: Date;
  updatedAt?: Date;
};
