import { AvailabilityElement, AvailabilityProps } from './Availability.types';
import styles from './Availability.module.css';
import classNames from 'classnames';
import addQuarters from 'date-fns/addQuarters';
import getQuarter from 'date-fns/getQuarter';
import format from 'date-fns/format';
import isPast from 'date-fns/isPast';

export default function Availability({
  as: Element = AvailabilityElement.ASIDE,
  className,
  isAvailableNow = false,
  isAvailableAt = null,
}: AvailabilityProps) {
  const now = new Date();

  let nextQuarterDate: Date;

  if (isAvailableNow) {
    nextQuarterDate = now;
  } else if (isAvailableAt && !isPast(isAvailableAt)) {
    nextQuarterDate = isAvailableAt;
  } else {
    nextQuarterDate = addQuarters(now, 1);
  }

  const nextQuarter = getQuarter(nextQuarterDate);
  const nextQuarterYear = format(nextQuarterDate, 'yyyy');

  return (
    <Element className={classNames(styles.Availability, className)}>
      <p>
        Accepting new projects for:
        <strong className={styles.Availability__period}>
          {`Q${nextQuarter} ${nextQuarterYear}`}
        </strong>
      </p>
    </Element>
  );
}
