import { ClassName } from '@/lib/types';

export type AvailabilityProps = {
  as?: AvailabilityElement;
  className?: ClassName;
  isAvailableNow?: boolean;
  isAvailableAt?: Date | null;
};

export enum AvailabilityElement {
  ASIDE = 'aside',
  DIV = 'div',
}
