import PageSection, {
  Header as PageSectionHeader,
  PageSectionHeaderAlignment,
  PageSectionType,
  TitleLevel,
} from '@/components/layout/PageSection';
import ImageGrid from './ImageGrid';
import Image from 'next/image';
import styles from './Ecologi.module.css';
import Button, { ButtonLevel } from '@/components/shared/Button';
import Logo, { LogoType } from '@/components/shared/Logo';

export default function Ecologi({ trees = 1000 }) {
  return (
    <PageSection id="ecologi" variant={PageSectionType.SPLIT_REVERSED}>
      <div>
        <div className={styles.Ecologi__content}>
          <PageSectionHeader titleLevel={TitleLevel.H2} aligned={PageSectionHeaderAlignment.LEFT}>
            <div className={styles.Ecologi__partnered}>
              <Image src="/images/logos/ecologi-name.svg" alt="Ecologi" width="75" height="46" />
              <Logo variant={LogoType.TWO_LINES} className={styles.Ecologi__logo} />
            </div>
            We&apos;ve partnered with Ecologi
          </PageSectionHeader>
          <div className={styles.Ecologi__copy}>
            <p>
              Increment By One are committed to sustainable development. A portion of every project
              we complete will be donated to ecological charities.
            </p>
            <p className={styles.Ecologi__trees}>
              <span className={styles.Egologi__tree}>🌱</span> Over{' '}
              {Intl.NumberFormat('en-GB').format(trees)} trees planted so far!
            </p>
          </div>
          <Button href="https://ecologi.com/incrementbyone" level={ButtonLevel.SECONDARY}>
            View Ecologi Profile
          </Button>
        </div>
      </div>

      <div>
        <ImageGrid
          primary={
            <Image
              src="/images/ecologi/climate-action-workforce.jpg"
              alt="A view of a forest pathway"
              layout="fill"
            />
          }
          supporting={[
            <Image
              key="seeds"
              src="/images/ecologi/seeds.jpg"
              alt="A person holding a mixture of tree seeds in their cupped hands."
              layout="fill"
            />,
            <Image
              key="windmills"
              src="/images/ecologi/windmills.jpg"
              alt="A view over a green, hilly landscape showing two power-generating windmills."
              layout="fill"
            />,
          ]}
        />
      </div>
    </PageSection>
  );
}
