import { ImageGridProps } from './ImageGrid.types';
import styles from './ImageGrid.module.css';
import classNames from 'classnames';

export default function ImageGrid({ primary, supporting }: ImageGridProps) {
  return (
    <figure className={styles.ImageGrid}>
      <div className={classNames(styles.ImageGridItem, styles['ImageGridItem--primary'])}>
        {primary}
      </div>
      {supporting.slice(0, 2).map((image) => (
        <div key={image.key} className={styles.ImageGridItem}>
          {image}
        </div>
      ))}
    </figure>
  );
}
