import { ReactElement } from 'react';

export type ImageGridProps = {
  primary: ReactElement;
  supporting: ReactElement[];
};
