import PageSection, {
  Header as PageSectionHeader,
  PageSectionType,
  PageSectionHeaderSize,
  TitleLevel,
} from '@/components/layout/PageSection';
import Image from 'next/image';
import styles from './WhyTheName.module.css';

export default function WhyTheName() {
  return (
    <PageSection className={styles.WhyTheName} variant={PageSectionType.SPLIT_REVERSED}>
      <div className={styles.WhyTheName__content}>
        <div className={styles.WhyTheName__contentInner}>
          <PageSectionHeader size={PageSectionHeaderSize.MEDIUM} titleLevel={TitleLevel.H2}>
            Why the name?
          </PageSectionHeader>

          <p>
            Increment By One is all about improvement. It&apos;s a mindset that sets us apart: the
            idea that we always leave projects better than they were. Even if it&apos;s the best
            application in the world, there&apos;ll still be ways to improve upon it.
          </p>
        </div>
      </div>
      <div>
        <figure className={styles.WhyTheName__image}>
          <Image src="/images/group-computers.jpg" alt="" layout="fill" />
        </figure>
      </div>
    </PageSection>
  );
}
