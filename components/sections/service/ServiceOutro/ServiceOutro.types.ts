import { PropsWithChildren } from 'react';

export type ServiceOutroProps = PropsWithChildren<{
  title: string;
  description?: string;
  linkText: string;
  url: string;
}>;
