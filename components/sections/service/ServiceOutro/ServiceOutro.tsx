import styles from './ServiceOutro.module.css';
import Button, { ButtonSize } from '@/components/shared/Button';
import { ServiceOutroProps } from './ServiceOutro.types';

export default function ServiceOutro({ title, children, linkText, url }: ServiceOutroProps) {
  return (
    <div className={styles.ServiceOutro}>
      <div className={styles.ServiceOutro__main}>
        <div className={styles.ServiceOutro__inner}>
          <h2 className={styles.ServiceOutro__title}>{title}</h2>

          <div className={styles.ServiceOutro__copy}>{children}</div>

          <Button size={ButtonSize.MEDIUM} href={url} className={styles.ServiceOutro__link}>
            {linkText}
          </Button>
        </div>
      </div>
    </div>
  );
}
