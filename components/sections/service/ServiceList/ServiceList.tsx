import PageSection, { PageSectionType } from '@/components/layout/PageSection';
import styles from './ServiceList.module.css';
import Button, { ButtonSize } from '@/components/shared/Button';
import List from './List';
import { ServiceListProps } from './ServiceList.types';

export default function ServiceList({ title, description, children }: ServiceListProps) {
  return (
    <PageSection variant={PageSectionType.SPLIT}>
      <div>
        <h2 className={styles.ServiceList__title}>{title}</h2>
        <div className={styles.ServiceList__copy}>{description}</div>
        <Button size={ButtonSize.MEDIUM} href="/contact-us" className={styles.ServiceList__link}>
          Get In Touch
        </Button>
      </div>
      <div>
        <List>{children}</List>
      </div>
    </PageSection>
  );
}
