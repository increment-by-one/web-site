import { ListItemProps } from '../ServiceIntro/ServiceIntro.types';
import styles from './ListItem.module.css';

export default function ListItem({ title, children }: ListItemProps) {
  return (
    <li className={styles.ListItem}>
      <p className={styles.ListItem__title}>{title}</p>
      <div className={styles.ListItem__copy}>{children}</div>
    </li>
  );
}
