import styles from './List.module.css';
import { ListProps } from './ServiceList.types';

export default function ListItem({ children }: ListProps) {
  return <ul className={styles.List}>{children}</ul>;
}
