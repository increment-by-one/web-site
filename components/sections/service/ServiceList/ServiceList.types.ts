import { PropsWithChildren } from 'react';

export type ListProps = PropsWithChildren<{}>;

export type ServiceListProps = PropsWithChildren<{
  title: string;
  description: string;
}>;
