import PageSection, {
  Header as PageSectionHeader,
  PageSectionHeaderAlignment,
  PageSectionType,
} from '@/components/layout/PageSection';
import styles from './ServiceIntro.module.css';
import { ServiceIntroProps } from './ServiceIntro.types';
import Button, { ButtonLevel } from '@/components/shared/Button';

export default function ServiceIntro({
  title,
  subTitle,
  children,
  illustration,
}: ServiceIntroProps) {
  return (
    <PageSection className={styles.ServiceIntro} variant={PageSectionType.SPLIT_REVERSED}>
      <div>
        <PageSectionHeader aligned={PageSectionHeaderAlignment.LEFT}>{title}</PageSectionHeader>
        <h2 className={styles.ServiceIntro__subtitle}>{subTitle}</h2>
        <div className={styles.ServiceIntro____copy}>{children}</div>
        <Button href="/contact-us" level={ButtonLevel.PLAIN}>
          Book a call
        </Button>
      </div>
      <div className={styles.ServiceIntro__illustration}>
        <div className={styles.ServiceIntro__inner}>{illustration}</div>
      </div>
    </PageSection>
  );
}
