import { PropsWithChildren } from 'react';

export type ListItemProps = PropsWithChildren<{ title: string }>;

export type ServiceIntroProps = PropsWithChildren<{
  title: string;
  subTitle: string;
  illustration: JSX.Element;
}>;
