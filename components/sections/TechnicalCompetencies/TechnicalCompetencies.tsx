import PageSection, {
  Header as PageSectionHeader,
  PageSectionType,
  TitleLevel,
  PageSectionHeaderAlignment,
} from '@/components/layout/PageSection';
import Image from 'next/image';

import CompetencyList from '@/components/shared/CompetencyList';
import Competency from '@/components/shared/CompetencyList/Competency';
import TechnologyLogos from '@/components/home/TechnologyLogos';
import DecoratedImage from '@/components/shared/DecoratedImage';

import styles from './TechnicalCompetencies.module.css';

export default function WhyTheName() {
  return (
    <div className={styles.TechnicalCompetencies}>
      <div className={styles.TechnicalCompetencies__inner}>
        <div className={styles.TechnicalCompetencies__content}>
          <PageSectionHeader
            titleLevel={TitleLevel.H2}
            aligned={PageSectionHeaderAlignment.LEFT}
            badge="Technical Competencies?"
          >
            We&apos;ll do you one better
          </PageSectionHeader>

          <p>
            We&apos;re a full stack engineering team with rich experience in all leading
            technologies. We work in a wide variety of coding languages and have both front and
            backend experience, so we know the ins and outs of how technology, data and design must
            work together to drive user engagement. We work differently, offering our clients
          </p>

          <CompetencyList>
            <Competency title="An empathy-led approach">
              That understands your initial needs and then increments to deliver a superior finished
              web application or software product.
            </Competency>
            <Competency title="Full proficiency in a wide range of technical languages">
              Including Node.js, Laravel, PHP, React – whatever you need to have built or refined,
              we can work with it.
            </Competency>
            <Competency title="Reliable Partnerships">
              With a team that only starts work once we have built the right level of understanding
              to know exactly how we’ll build your product
            </Competency>
          </CompetencyList>
        </div>
        <aside className={styles.TechnicalCompetencies__image}>
          <div className={styles.TechnicalCompetencies__img}>
            <DecoratedImage>
              <Image src="/images/laptop-code.jpg" width={580} height={440} alt="" />
            </DecoratedImage>
          </div>

          <TechnologyLogos />
        </aside>
      </div>
    </div>
  );
}
