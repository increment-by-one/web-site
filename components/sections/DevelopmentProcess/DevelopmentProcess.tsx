import Process from './Process';
import PageSection, {
  Header as PageSectionHeader,
  PageSectionHeaderAlignment,
  PageSectionType,
  TitleLevel,
} from '@/components/layout/PageSection';
import styles from './DevelopmentProcess.module.css';
import CallToAction from '@/components/shared/CallToAction';

export default function DevelopmentProcessSection() {
  return (
    <PageSection className={styles.DevelopmentProcess} variant={PageSectionType.INVERTED_LIGHT}>
      <PageSectionHeader
        titleLevel={TitleLevel.H2}
        badge="Our Process"
        aligned={PageSectionHeaderAlignment.LEFT}
      >
        Development with empathy
      </PageSectionHeader>

      <div className={styles.DevelopmentProcess__intro}>
        <p>
          We put humanity back into the machine-led world of development. With our experience in
          both front and backend systems, we know the ins and outs of how things should work. That
          gives us the ability to gauge your requirements on a technical and practical level, which
          we can then translate into plain-speaking projects that are built for user need, not just
          to meet stakeholder demand.
        </p>
      </div>

      <Process className={styles.DevelopmentProcess__process} />

      <CallToAction className={styles.DevelopmentProcess__CTA}>
        If you like what you hear, why not get in touch? We’re always happy to hear about technical
        challenges or ideas for web applications. And if you still want to put faces to names, take
        a look below.
      </CallToAction>
    </PageSection>
  );
}
