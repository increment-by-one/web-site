import Step from './Step';
import styles from './Process.module.css';
import { ClassName } from '@/lib/types';
import classNames from 'classnames';

export default function Process({ className }: { className?: ClassName }) {
  return (
    <ol className={classNames(styles.Process, className)} data-role="list">
      <li className={styles.Process__step}>
        <Step title="Getting to know us:" imageUrl="/images/illustrations/about-friends-wave.svg">
          <p>
            We&apos;ll chat to you to either take your existing brief or discuss a new project. If
            we think we&apos;re a good fit, we&apos;ll need to go deeper.
          </p>
        </Step>
      </li>
      <li className={styles.Process__step}>
        <Step
          title="Deep understanding:"
          imageUrl="/images/illustrations/about-deep-understanding.svg"
        >
          <p>
            We don&apos;t rush into a project head-on to meet any milestones or try and bill you
            ASAP. Instead, we&apos;ll talk through your project and company, your existing tech team
            and users – all so we can be confident before we write a single line of code.
          </p>
        </Step>
      </li>
      <li className={styles.Process__step}>
        <Step
          title="Delivery with a difference:"
          imageUrl="/images/illustrations/about-delivery-difference.svg"
        >
          <p>
            We&apos;re an outsourced team, so we&apos;re familiar with working any way you need us
            to. Whether that&apos;s dropping us into your existing development structure or setting
            up something new, we&apos;ll make an effort to deliver your project with minimal
            friction and maximum ease.
          </p>
        </Step>
      </li>
      <li className={styles.Process__step}>
        <Step
          title="Improvement as standard:"
          imageUrl="/images/illustrations/about-improvement-standard.svg"
        >
          <p>
            Before we hand a project back over, or even if we&apos;re going to continue looking
            after it, we&apos;ll always look to improve on what came before and deliver real value
            for future developers or your team to work with.
          </p>
        </Step>
      </li>
    </ol>
  );
}
