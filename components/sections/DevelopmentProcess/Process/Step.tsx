import Image from 'next/image';
import { PropsWithChildren } from 'react';
import styles from './Step.module.css';

type StepProps = {
  imageUrl: string;
  title: string;
};

export default function Step({ children, imageUrl, title }: PropsWithChildren<StepProps>) {
  return (
    <div className={styles.Step}>
      <Image className={styles.Step__image} src={imageUrl} width={80} height={80} alt="" />
      <h2 className={styles.Step__title}>{title}</h2>

      <div className={styles.Step__content}>{children}</div>
    </div>
  );
}
