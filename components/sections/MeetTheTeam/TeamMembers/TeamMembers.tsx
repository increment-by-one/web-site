import TeamMember from './TeamMember';
import styles from './TeamMembers.module.css';
import { TeamMemberList } from './TeamMembers.types';

export default function TeamMembers() {
  return (
    <ol className={styles.TeamMembers} data-role="list">
      {TeamMemberList.map((memberProps) => (
        <li key={memberProps.slug} className={styles.TeamMembers__item}>
          <TeamMember {...memberProps} />
        </li>
      ))}
    </ol>
  );
}
