export type TeamMemberProfile = {
  slug: string;
  name: string;
  givenName: string;
  title: string;
  imageUrl: string;
  social: string;
};

export type TeamMembersProps = {};

export type TeamMemberProps = TeamMemberProfile;

export const TeamMemberList: TeamMemberProfile[] = [
  // {
  //   slug: 'maria-arocha',
  //   name: 'Maria Arocha',
  //   givenName: 'Maria',
  //   title: 'Web Developer',
  //   imageUrl: '/images/team/maria-arocha.jpg',
  //   social: 'https://social.incrementby.one/@maria',
  // } as TeamMemberProfile,
  {
    slug: 'jon-park',
    name: 'Jon Park',
    givenName: 'Jon',
    title: 'Lead Developer',
    imageUrl: '/images/team/jon-park.jpg',
    social: 'https://howdee.social/@jonspark',
  } as TeamMemberProfile,
];
