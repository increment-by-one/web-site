import Mastodon from '@/components/shared/icons/Mastodon';
import Image from 'next/image';
import Link from 'next/link';
import styles from './TeamMember.module.css';
import { TeamMemberProps } from './TeamMembers.types';

export default function TeamMember({ givenName, imageUrl, name, title, social }: TeamMemberProps) {
  return (
    <article className={styles.TeamMember}>
      <figure className={styles.TeamMember__image}>
        <Image src={imageUrl} layout="fill" alt="" />
      </figure>

      <h1 className={styles.TeamMember__name}>{name}</h1>
      <p>{title}</p>
      <Link href={social}>
        <a rel="me">
          <Mastodon className={styles.TeamMember__icon} />
          <span className="is-visually-hidden">Follow ${givenName} on Mastodon.</span>
        </a>
      </Link>
    </article>
  );
}
