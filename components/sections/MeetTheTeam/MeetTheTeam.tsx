import PageSection, {
  PageSectionHeaderSize,
  TitleLevel,
  PageSectionType,
} from '@/components/layout/PageSection';
import TeamMembers from './TeamMembers';

export default function MeetTheTeam() {
  return (
    <PageSection
      title="Meet The Team"
      titleLevel={TitleLevel.H2}
      titleSize={PageSectionHeaderSize.MEDIUM}
      variant={PageSectionType.HIGHLIGHT}
      id="meet-the-team"
    >
      <TeamMembers />
    </PageSection>
  );
}
