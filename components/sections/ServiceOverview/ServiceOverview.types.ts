import servicesContent from '@/content/services';

type ServiceKey =
  | 'development-consultancy'
  | 'server-side-development'
  | 'web-application-development';

export type ServiceOverviewProps = {
  service: ServiceKey;
};
