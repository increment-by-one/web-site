import CallToAction from '@/components/shared/CallToAction';
import servicesContent from '@/content/services';
import Image from 'next/image';
import { ServiceOverviewProps } from './ServiceOverview.types';
import styles from './ServiceOverview.module.css';
import Competency from '@/components/shared/CompetencyList/Competency';
import CompetencyList from '@/components/shared/CompetencyList';
import PageSection, { PageSectionType } from '@/components/layout/PageSection';

export default function ServiceOverview({ service }: ServiceOverviewProps) {
  const { title, overview, details, callToAction } = servicesContent[service];

  return (
    <PageSection variant={PageSectionType.HIGHLIGHT}>
      <div className={styles.ServiceOverview}>
        <div className={styles.ServiceOverview__inner}>
          <div className={styles.ServiceOverview__description}>
            <figure className={styles.ServiceOverview__image}>
              <Image
                className={styles.ServiceOverview__img}
                src={`/images/illustrations/${service}.svg`}
                layout="fill"
                alt=""
              />
            </figure>
            <h2 className={styles.ServiceOverview__title}>{title}</h2>
            <p className={styles.ServiceOverview__copy}>{overview}</p>
          </div>
          <div className={styles.Details}>
            <h2 className={styles.Details__title}>{details.title}</h2>
            <CompetencyList>
              {details.items.map(({ content, title }, i) => (
                <Competency key={i} title={title}>
                  {content}
                </Competency>
              ))}
            </CompetencyList>
          </div>
        </div>
        <CallToAction
          className={styles.ServiceOverview__CTA}
          label={callToAction.button}
          url={`/our-services/${service}`}
        >
          {callToAction.text}
        </CallToAction>
      </div>
    </PageSection>
  );
}
