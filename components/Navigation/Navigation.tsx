import classNames from 'classnames';
import Link from 'next/link';
import styles from './Navigation.module.css';
import { NavigationProps, NavigationVariant } from './Navigation.types';

export default function Navigation({ className, variant, withHome = true }: NavigationProps) {
  return (
    <nav
      className={classNames(styles.Navigation, className, {
        [styles[`Navigation--${variant}`]]: variant !== NavigationVariant.DEFAULT,
      })}
    >
      <ul className={styles.Navigation__items} data-role="list">
        {withHome && (
          <li className={styles.Navigation__item}>
            <Link href="/">
              <a className={styles.Navigation__link}>Home</a>
            </Link>
          </li>
        )}
        <li className={styles.Navigation__item}>
          <Link href="/about">
            <a className={styles.Navigation__link}>About</a>
          </Link>
        </li>
        <li className={styles.Navigation__item}>
          <Link href="/our-services">
            <a className={styles.Navigation__link}>Our Services</a>
          </Link>
        </li>
        <li className={styles.Navigation__item}>
          <Link href="/our-services">
            <a className={styles.Navigation__link}>Our Work</a>
          </Link>
        </li>
        <li className={styles.Navigation__item}>
          <Link href="/news">
            <a className={styles.Navigation__link}>News</a>
          </Link>
        </li>
        <li className={styles.Navigation__item}>
          <Link href="/contact-us">
            <a className={styles.Navigation__link}>Get In Touch</a>
          </Link>
        </li>
      </ul>
    </nav>
  );
}
