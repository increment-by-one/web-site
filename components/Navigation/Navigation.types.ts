import { ClassName } from '@/lib/types';

export type NavigationProps = {
  className?: ClassName;
  variant?: NavigationVariant;
  withHome?: boolean;
};

export enum NavigationVariant {
  DEFAULT = 'default',
  HEADER = 'header',
}
