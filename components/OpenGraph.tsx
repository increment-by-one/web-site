export default function OpenGraph({
  description,
  image,
  title,
  url,
}: {
  description: string;
  image: string;
  title: string;
  url: string;
}) {
  return [
    <meta key="description" property="description" content={description} />,
    <meta key="image" name="image" content={image} />,

    <meta key="og:type" property="og:type" content="website" />,
    <meta key="og:title" property="og:title" content={title} />,
    <meta key="og:description" property="og:description" content={description} />,
    <meta key="og:url" property="og:url" content={url} />,
    <meta key="og:image" property="og:image" content={image} />,

    <meta key="twitter:card" property="twitter:card" content="summary_large_image" />,
    <meta key="twitter:title" property="twitter:title" content={title} />,
    <meta key="twitter:description" property="twitter:description" content={description} />,
    <meta key="twitter:image" property="twitter:image" content={image} />,
  ];
}
