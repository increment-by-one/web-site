import { ClassName } from '@/lib/types';
import { PropsWithChildren } from 'react';

export type CallToActionProps = PropsWithChildren<{
  className?: ClassName;
  label?: string;
  url?: string;
}>;
