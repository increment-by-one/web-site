import Button, { ButtonLevel, ButtonSize } from '@/components/shared/Button';
import classNames from 'classnames';
import styles from './CallToAction.module.css';
import { CallToActionProps } from './CallToAction.types';

export default function CallToAction({
  children,
  className,
  label = 'Get in touch now',
  url = '/contact',
}: CallToActionProps) {
  return (
    <div className={classNames(styles.CallToAction, className)}>
      <p className={styles.CallToAction__text}>{children}</p>
      <Button
        href={url}
        level={ButtonLevel.TERTIARY}
        size={ButtonSize.LARGE}
        className={styles.CallToAction__button}
      >
        {label}
      </Button>
    </div>
  );
}
