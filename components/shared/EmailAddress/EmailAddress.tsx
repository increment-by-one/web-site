import { ClassName } from '@/lib/types';
import classNames from 'classnames';
import EnvelopeIcon from '@/components/shared/icons/Envelope';
import styles from './EmailAddress.module.css';

export default function EmailAddress({ className }: { className?: ClassName }) {
  return (
    <a className={classNames(styles.EmailAddress, className)} href="mailto:hey@incrementby.one">
      <EnvelopeIcon className={styles.EmailAddress__envelope} />
      hey@incrementby.one
    </a>
  );
}
