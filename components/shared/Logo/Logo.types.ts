import { ClassName } from '@/lib/types';
import { SVGProps } from 'react';

export type LogoProps = SVGProps<SVGSVGElement> & {
  className?: ClassName;
  isInverted?: boolean;
  variant?: LogoType;
};

export enum LogoType {
  DEFAULT = 'default',
  TWO_LINES = 'two-lines',
}
