import Button, { ButtonLevel, ButtonSize } from '../Button';
import FastEmail from '../illustrations/FastEmail';
import styles from './NextProject.module.css';

export default function NextProject() {
  return (
    <div className={styles.NextProject}>
      <div className={styles.NextProject__leading}>
        <FastEmail className={styles.NextProject__icon} aria-hidden />

        <div className={styles.NextProject__text}>
          <p className={styles.NextProject__title}>Looking to start your next project?</p>
          <p>We&apos;re here to help take your project to the next level.</p>
        </div>
      </div>

      <div className={styles.NextProject__trailing}>
        <Button
          className={styles.NextProject__button}
          href="/contact-us"
          level={ButtonLevel.TERTIARY}
          size={ButtonSize.LARGE}
        >
          Get in touch
        </Button>
      </div>

      <div className={styles.NextProject__shade} aria-hidden />
    </div>
  );
}
