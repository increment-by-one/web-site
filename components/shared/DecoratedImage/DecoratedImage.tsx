import styles from './DecoratedImage.module.css';
import { PropsWithChildren } from 'react';

export default function DecoratedImage({ children }: PropsWithChildren) {
  return (
    <div className={styles.DecoratedImage}>
      <figure className={styles.DecoratedImage__inner}>{children}</figure>
    </div>
  );
}
