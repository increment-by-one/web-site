import { PropsWithChildren } from 'react';
import styles from './Badge.module.css';

export default function Badge({ children }: PropsWithChildren<{}>) {
  return <span className={styles.Badge}>{children}</span>;
}
