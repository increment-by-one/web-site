import Link from 'next/link';
import { ButtonLevel, ButtonProps, ButtonSize } from './Button.types';
import styles from './Button.module.css';
import classNames from 'classnames';

export default function Button({
  children,
  className,
  href,
  level = ButtonLevel.PRIMARY,
  size = ButtonSize.DEFAULT,
  type = 'button',
}: ButtonProps) {
  const allClasses = classNames(
    styles.Button,
    {
      [styles[`Button--${level}`]]: level !== ButtonLevel.PRIMARY,
      [styles[`Button--${size}`]]: size !== ButtonSize.DEFAULT,
    },
    className
  );

  if (href) {
    return (
      <Link href={href}>
        <a className={allClasses}>
          <span>{children}</span>
        </a>
      </Link>
    );
  }

  return (
    <button className={allClasses} type={type}>
      <span>{children}</span>
    </button>
  );
}
