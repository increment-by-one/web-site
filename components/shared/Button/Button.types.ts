import { ClassName } from '@/lib/types';
import { PropsWithChildren } from 'react';

export enum ButtonLevel {
  PRIMARY = 'primary',
  SECONDARY = 'secondary',
  TERTIARY = 'tertiary',
  PLAIN = 'plain',
}

export type ButtonProps = PropsWithChildren<{
  className?: ClassName;
  href?: string;
  level?: ButtonLevel;
  size?: ButtonSize;
  type?: 'button' | 'submit';
}>;

export enum ButtonSize {
  DEFAULT = 'default',
  LARGE = 'large',
  MEDIUM = 'medium',
}
