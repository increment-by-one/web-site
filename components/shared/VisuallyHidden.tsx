type VisuallyHiddenProps = React.PropsWithChildren<{
  as?: 'span';
}>;

export default function VisuallyHidden({
  as: Element = 'span',
  children,
}: VisuallyHiddenProps) {
  return <Element className="is-visually-hidden">{children}</Element>;
}
