import { ClassName } from '@/lib/types';
import classNames from 'classnames';

export default function Devices({ className }: { className?: ClassName }) {
  return (
    <svg
      className={classNames(className)}
      style={{ fill: 'none' }}
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 100 59"
    >
      <path
        opacity=".3"
        d="M1.992 55.36c0 .75 5.136 1.468 14.28 1.997 9.143.53 21.543.827 34.474.827 12.93 0 25.33-.297 34.474-.827 9.143-.53 14.28-1.248 14.28-1.997 0-.749-5.137-1.467-14.28-1.997s-21.544-.827-34.474-.827c-12.93 0-25.331.298-34.475.827-9.143.53-14.28 1.248-14.28 1.997Z"
        fill="#8F5AFF"
      />
      <path d="M58.632 45.07H68.3v5.8h-9.668v-5.8Z" fill="#fff" />
      <path
        d="M58.632 45.07H68.3v5.8h-9.668v-5.8Z"
        stroke="#201C3E"
        stroke-linecap="round"
        stroke-linejoin="round"
      />
      <path
        d="M75.837 53.511a.932.932 0 0 1-.883 1.225H51.978a.932.932 0 0 1-.882-1.225 3.868 3.868 0 0 1 3.669-2.643h17.4a3.87 3.87 0 0 1 3.672 2.643v0Z"
        fill="#fff"
        stroke="#201C3E"
        stroke-linecap="round"
        stroke-linejoin="round"
      />
      <path
        d="M93.437 5.43A4.834 4.834 0 0 0 88.6.594H38.33a4.835 4.835 0 0 0-4.835 4.834v31.9h59.942V5.43Z"
        fill="#8F5AFF"
      />
      <path
        d="M93.437 5.43A4.834 4.834 0 0 0 88.6.594H38.33a4.835 4.835 0 0 0-4.835 4.834v31.9h59.942V5.43Z"
        stroke="#201C3E"
        stroke-linecap="round"
        stroke-linejoin="round"
      />
      <path
        d="M88.6 45.068a4.834 4.834 0 0 0 4.834-4.834v-2.9H33.495v2.9a4.834 4.834 0 0 0 4.834 4.834H88.6Z"
        fill="#fff"
      />
      <path
        d="M88.6 45.068a4.834 4.834 0 0 0 4.834-4.834v-2.9H33.495v2.9a4.834 4.834 0 0 0 4.834 4.834H88.6Z"
        stroke="#201C3E"
        stroke-linecap="round"
        stroke-linejoin="round"
      />
      <path d="M64.4 41.199a.93.93 0 1 1-1.86 0 .93.93 0 0 1 1.86 0Z" fill="#201C3E" />
      <path d="M80.985 22.116h17.793v32.62H80.985v-32.62Z" fill="#fff" />
      <path
        d="M80.985 22.116h17.793v32.62H80.985v-32.62Z"
        stroke="#201C3E"
        stroke-linecap="round"
        stroke-linejoin="round"
      />
      <path d="M80.985 28.049h17.793v19.275H80.985V28.049Z" fill="#8F5AFF" />
      <path
        d="M80.985 28.049h17.793v19.275H80.985V28.049ZM88.398 25.083h2.966"
        stroke="#201C3E"
        stroke-linecap="round"
        stroke-linejoin="round"
      />
      <path d="M88.951 51.029a.93.93 0 1 0 1.86 0 .93.93 0 0 0-1.86 0Z" fill="#201C3E" />
      <path
        d="M54.153 18.875a8.752 8.752 0 0 1 15.217-5.912l3.442-3.443A13.619 13.619 0 1 0 53.56 28.771L57 25.329a8.731 8.731 0 0 1-2.847-6.454v0ZM70.115 23.862a8.778 8.778 0 0 1-3.058 2.725L70.6 30.13a13.676 13.676 0 0 0 2.99-2.793l-3.475-3.474ZM64.961 32.334v-4.948a8.72 8.72 0 0 1-3.987.032c-.035-.008-.068-.02-.1-.029v4.945c1.354.227 2.736.227 4.09 0h-.003ZM71.461 20.814a8.977 8.977 0 0 1-.22.785h5.032c.272-1.345.34-2.724.2-4.089h-4.906a8.727 8.727 0 0 1-.106 3.304v0ZM84.382 38.236a5.5 5.5 0 1 0 11 0 5.5 5.5 0 0 0-11 0v0Zm4.818-2.978a3.054 3.054 0 1 1 1.356 5.956 3.054 3.054 0 0 1-1.356-5.956v0Z"
        fill="#fff"
        stroke="#201C3E"
        stroke-linecap="round"
        stroke-linejoin="round"
      />
      <path
        d="M84.382 38.24a5.5 5.5 0 0 0 10.58 2.104 5.498 5.498 0 1 0-10.58-2.104v0Z"
        stroke="#201C3E"
        stroke-linecap="round"
        stroke-linejoin="round"
      />
      <path
        d="M93.24 33.885a5.5 5.5 0 1 0-6.7 8.723l1.493-1.937a3.054 3.054 0 0 1 3.608-4.928l1.599-1.858Z"
        fill="#fff"
        stroke="#201C3E"
        stroke-linecap="round"
        stroke-linejoin="round"
      />
      <path
        d="M41.015 54.693H8.268a8.988 8.988 0 0 1-7.562-4.092 1.258 1.258 0 0 1 1.053-1.947h45.764a1.259 1.259 0 0 1 1.053 1.947 8.988 8.988 0 0 1-7.561 4.092Z"
        fill="#fff"
      />
      <path
        d="M46.54 26a3.021 3.021 0 0 0-3.02-3.021H5.763a3.02 3.02 0 0 0-3.02 3.02v22.652h43.8L46.538 26Z"
        fill="#fff"
        stroke="#201C3E"
        stroke-linecap="round"
        stroke-linejoin="round"
      />
      <path
        d="M42.009 48.65H7.274V28.138a.63.63 0 0 1 .629-.63h33.476a.63.63 0 0 1 .63.63V48.65Z"
        fill="#8F5AFF"
      />
      <path
        d="M41.015 54.693H8.268a8.988 8.988 0 0 1-7.562-4.092 1.258 1.258 0 0 1 1.053-1.947h45.764a1.259 1.259 0 0 1 1.053 1.947 8.988 8.988 0 0 1-7.561 4.092v0ZM42.009 48.65H7.274V28.138a.63.63 0 0 1 .629-.63h33.476a.63.63 0 0 1 .63.63V48.65ZM15.58 51.67h-2.265M10.294 51.67h-3.02"
        stroke="#201C3E"
        stroke-linecap="round"
        stroke-linejoin="round"
      />
      <path
        d="M25.088 30.425a7.7 7.7 0 1 0 0 15.4 7.7 7.7 0 0 0 0-15.4v0Zm4.17 8.647a4.276 4.276 0 1 1-8.34-1.894 4.276 4.276 0 0 1 8.34 1.895v0Z"
        fill="#fff"
        stroke="#201C3E"
        stroke-linecap="round"
        stroke-linejoin="round"
      />
      <path
        d="M17.389 38.124a7.7 7.7 0 1 0 15.399 0 7.7 7.7 0 0 0-15.4 0v0Z"
        stroke="#201C3E"
        stroke-linecap="round"
        stroke-linejoin="round"
      />
      <path
        d="M22.626 34.625a4.277 4.277 0 0 1 5.052 6.9l2.09 2.711a7.699 7.699 0 0 0-6.68-13.587c-.983.275-1.902.743-2.702 1.376l2.24 2.6Z"
        fill="#fff"
        stroke="#201C3E"
        stroke-linecap="round"
        stroke-linejoin="round"
      />
    </svg>
  );
}
