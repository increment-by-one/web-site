import { ClassName } from '@/lib/types';
import classNames from 'classnames';

export default function Consultancy({ className }: { className?: ClassName }) {
  return (
    <svg
      className={classNames(className)}
      style={{ fill: 'none' }}
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 67 93"
    >
      <path
        d="M26.885 71.283v20.146h4.96l.437-13.471a1.77 1.77 0 0 1 3.539 0l.435 13.47h4.962V71.284H26.885Z"
        fill="#8F5AFF"
      />
      <path
        d="M26.885 71.283v20.146h4.96l.437-13.471a1.77 1.77 0 0 1 3.539 0l.435 13.47h4.962V71.284H26.885Z"
        stroke="#201C3E"
        stroke-linecap="round"
        stroke-linejoin="round"
      />
      <path
        d="M45.628 62.456a11.577 11.577 0 0 0-23.153 0v9.371a.552.552 0 0 0 .551.552h3.86v5.517h3.6a4 4 0 0 0 3.572-2.2 3.98 3.98 0 0 0 3.562 2.2h3.6V72.38h3.858a.552.552 0 0 0 .552-.552l-.002-9.37Z"
        fill="#8F5AFF"
      />
      <path
        d="M45.628 62.456a11.577 11.577 0 0 0-23.153 0v9.371a.552.552 0 0 0 .551.552h3.86v5.517h3.6a4 4 0 0 0 3.572-2.2 3.98 3.98 0 0 0 3.562 2.2h3.6V72.38h3.858a.552.552 0 0 0 .552-.552l-.002-9.37Z"
        stroke="#201C3E"
        stroke-linecap="round"
        stroke-linejoin="round"
      />
      <path
        d="M23.026 74.31a1.93 1.93 0 0 0 3.859 0v-1.929h-3.86v1.929ZM41.218 74.31a1.929 1.929 0 1 0 3.858 0v-1.929h-3.858v1.929ZM34.05 50.882c-1.576-.001-3.135.321-4.582.947l4.583 12.834 4.584-12.834a11.537 11.537 0 0 0-4.584-.947Z"
        fill="#fff"
        stroke="#201C3E"
        stroke-linecap="round"
        stroke-linejoin="round"
      />
      <path
        d="m34.051 64.66 6.064-7.164-1.1-2.205h2.205l-.656-2.4a11.622 11.622 0 0 0-1.927-1.058L34.051 64.66Z"
        fill="#8F5AFF"
        stroke="#201C3E"
        stroke-linecap="round"
        stroke-linejoin="round"
      />
      <path
        d="m34.051 53.623-1.888 5.752 1.888 5.286 1.888-5.286-1.888-5.752ZM34.05 64.667l-4.582-12.834c-.675.29-1.32.645-1.927 1.058l-.656 2.4h2.205l-1.1 2.205 6.06 7.17Z"
        fill="#8F5AFF"
        stroke="#201C3E"
        stroke-linecap="round"
        stroke-linejoin="round"
      />
      <path
        d="M34.051 64.666v11.025"
        stroke="#201C3E"
        stroke-linecap="round"
        stroke-linejoin="round"
      />
      <path
        d="m36.452 51.126-2.401 2.5-2.38-2.5.006-4.086h4.78l-.005 4.086Z"
        fill="#fff"
        stroke="#201C3E"
        stroke-width="1.009"
        stroke-linecap="round"
        stroke-linejoin="round"
      />
      <path
        d="M41.769 43.27a1.361 1.361 0 0 0-1.1-1.323v-.882a6.615 6.615 0 0 0-13.23 0v.882a1.35 1.35 0 0 0 .139 2.673 6.615 6.615 0 0 0 12.952 0 1.364 1.364 0 0 0 1.239-1.35Z"
        fill="#fff"
        stroke="#201C3E"
        stroke-linecap="round"
        stroke-linejoin="round"
      />
      <path
        d="M30.744 41.893a.827.827 0 1 0 1.654 0 .827.827 0 0 0-1.654 0ZM35.705 41.893a.827.827 0 1 0 1.654 0 .827.827 0 0 0-1.654 0Z"
        fill="#201C3E"
      />
      <path
        d="M29.641 44.099a.827.827 0 1 0 1.654 0 .827.827 0 0 0-1.654 0ZM36.807 44.099a.827.827 0 1 0 1.654 0 .827.827 0 0 0-1.654 0Z"
        fill="#8F5AFF"
      />
      <path
        d="M32.4 46.08a2.984 2.984 0 0 0 3.307 0"
        stroke="#201C3E"
        stroke-linecap="round"
        stroke-linejoin="round"
      />
      <path
        d="M41.218 39.41a7.167 7.167 0 0 0-14.333 0v2.204a5.694 5.694 0 0 0 3.859-3.307c3.088 2.08 7.166 1.653 10.474 1.102Z"
        fill="#fff"
      />
      <path
        d="M41.218 39.41a7.167 7.167 0 0 0-14.333 0v2.204a5.694 5.694 0 0 0 3.859-3.307c3.088 2.08 7.166 1.653 10.474 1.102Z"
        stroke="#201C3E"
        stroke-linecap="round"
        stroke-linejoin="round"
      />
      <path
        d="M29.641 37.21c.28.444.656.82 1.1 1.1"
        stroke="#201C3E"
        stroke-linecap="round"
        stroke-linejoin="round"
      />
      <path d="m34.051 53.623-3.263 1.901 3.263-1.9Z" fill="#fff" />
      <path
        d="m34.051 53.623-3.263 1.901"
        stroke="#201C3E"
        stroke-linecap="round"
        stroke-linejoin="round"
      />
      <path d="m34.05 53.623 3.265 1.901-3.264-1.9Z" fill="#fff" />
      <path
        d="m34.05 53.623 3.265 1.901M26.885 72.382v-11.76M41.218 72.382v-11.76"
        stroke="#201C3E"
        stroke-linecap="round"
        stroke-linejoin="round"
      />
      <path
        d="M41.644 91.68a1 1 0 0 0 .9-1.433 3.308 3.308 0 0 0-2.981-1.874h-.551a3.307 3.307 0 0 0-2.982 1.874 1 1 0 0 0 .9 1.433h4.714ZM31.17 91.68a1 1 0 0 0 .9-1.433 3.309 3.309 0 0 0-2.982-1.874h-.55a3.308 3.308 0 0 0-2.983 1.874 1.001 1.001 0 0 0 .9 1.433h4.715Z"
        fill="#fff"
        stroke="#201C3E"
        stroke-linecap="round"
        stroke-linejoin="round"
      />
      <path d="m60.365 74.977-.497 7.958h-6.965l-.498-7.958h7.96Z" fill="#fff" />
      <path
        d="M62.92 47.24a1 1 0 0 1-1 1H50.782a1 1 0 0 1-1-1v-7.086H62.92v7.086ZM16.087 67.697a1.741 1.741 0 1 0 3.483 0v-10.2h-3.483v10.2Z"
        fill="#fff"
        stroke="#201C3E"
        stroke-linecap="round"
        stroke-linejoin="round"
      />
      <path
        d="M4.147 64.968v17.408h4.478l.393-11.383a1.6 1.6 0 0 1 3.194 0l.393 11.383h4.477V64.968H4.147Z"
        fill="#8F5AFF"
      />
      <path
        d="M4.147 64.968v17.408h4.478l.393-11.383a1.6 1.6 0 0 1 3.194 0l.393 11.383h4.477V64.968H4.147Z"
        stroke="#201C3E"
        stroke-linecap="round"
        stroke-linejoin="round"
      />
      <path
        d="M19.57 83.374a2.984 2.984 0 0 0-2.985-2.985h-3.98v1.985a1 1 0 0 0 1 1h5.965ZM11.113 83.374a2.984 2.984 0 0 0-2.986-2.985h-3.98v1.985a1 1 0 0 0 1 1h5.966Z"
        fill="#fff"
        stroke="#201C3E"
        stroke-linecap="round"
        stroke-linejoin="round"
      />
      <path
        d="M.665 56.013a9.453 9.453 0 0 1 18.905 0v1.493h-2.488v7.462H4.147v-7.462H.665v-1.493Z"
        fill="#fff"
      />
      <path
        d="M.665 56.013a9.453 9.453 0 0 1 18.905 0v1.493h-2.488v7.462H4.147v-7.462H.665v-1.493Z"
        stroke="#201C3E"
        stroke-linecap="round"
        stroke-linejoin="round"
      />
      <path
        d="M.665 67.697a1.741 1.741 0 0 0 3.482 0v-10.2H.665v10.2Z"
        fill="#fff"
        stroke="#201C3E"
        stroke-linecap="round"
        stroke-linejoin="round"
      />
      <path
        d="M4.147 57.507v-3.469M17.082 57.504v-2.985"
        stroke="#201C3E"
        stroke-linecap="round"
        stroke-linejoin="round"
      />
      <path
        d="M12.806 46.955a1 1 0 0 1-.714-.96v-2.69H8.187v2.68a1 1 0 0 1-.725.961 8.213 8.213 0 0 0-.279.085 3.467 3.467 0 0 0 6.806.363 9.257 9.257 0 0 0-1.183-.439Z"
        fill="#fff"
        stroke="#201C3E"
        stroke-width="1.009"
        stroke-linecap="round"
        stroke-linejoin="round"
      />
      <path
        d="M10.6 31.227a5.988 5.988 0 0 0-5.987 5.987v1.2c-1.709 0-1.592 2.994.3 2.994a5.97 5.97 0 0 0 11.67-1.8v-2.4a5.987 5.987 0 0 0-5.983-5.98Z"
        fill="#fff"
        stroke="#201C3E"
        stroke-linecap="round"
        stroke-linejoin="round"
      />
      <path
        d="M10.62 38.373a.75.75 0 1 1-1.5 0 .75.75 0 0 1 1.5 0ZM15.1 38.373a.75.75 0 1 1-1.498 0 .75.75 0 0 1 1.498 0Z"
        fill="#201C3E"
      />
      <path d="M9.046 40.95a.6.6 0 1 1-1.2 0 .6.6 0 0 1 1.2 0Z" fill="#8F5AFF" />
      <path
        d="M10.615 42.108a2.272 2.272 0 0 0 2.993 0"
        stroke="#201C3E"
        stroke-linecap="round"
        stroke-linejoin="round"
      />
      <path
        d="M16.72 36.609c1.2-.6.489-3.686-.6-4.19l.78-.622a.9.9 0 0 0-.62-1.63H8.936a4.67 4.67 0 0 0-4.79 4.647v3.592c3.592 0 3.516-2.824 6.586-3.592.08.7.971 1.355 2.394 1.8a1.759 1.759 0 0 1 1.8-1.8l1.793 1.795Z"
        fill="#fff"
      />
      <path
        d="M16.72 36.609c1.2-.6.489-3.686-.6-4.19l.78-.622a.9.9 0 0 0-.62-1.63H8.936a4.67 4.67 0 0 0-4.79 4.647v3.592c3.592 0 3.516-2.824 6.586-3.592.08.7.971 1.355 2.394 1.8a1.759 1.759 0 0 1 1.8-1.8l1.793 1.795ZM60.365 74.977l-.497 7.958h-6.965l-.498-7.958h7.96ZM56.385 74.977v7.958"
        stroke="#201C3E"
        stroke-linecap="round"
        stroke-linejoin="round"
      />
      <path
        d="M49.42 83.43a2.984 2.984 0 0 1 2.985-2.984h3.98v2.985H49.42Z"
        fill="#8F5AFF"
        stroke="#201C3E"
        stroke-linecap="round"
        stroke-linejoin="round"
      />
      <path
        d="M53.4 83.43a2.985 2.985 0 0 1 2.985-2.984h3.98v1.985a1 1 0 0 1-1 1H53.4Z"
        fill="#8F5AFF"
        stroke="#201C3E"
        stroke-linecap="round"
        stroke-linejoin="round"
      />
      <path d="m51.45 74.975-.537-9.95h10.945l-.538 9.95h-9.87Z" fill="#8F5AFF" />
      <path
        d="m51.45 74.975-.537-9.95h10.945l-.538 9.95h-9.87Z"
        stroke="#201C3E"
        stroke-linecap="round"
        stroke-linejoin="round"
      />
      <path d="M61.858 48.234a8.457 8.457 0 0 0-10.945.85v15.94h10.945v-16.79Z" fill="#ECEAF5" />
      <path
        d="M61.858 48.234a8.457 8.457 0 0 0-10.945.85v15.94h10.945v-16.79Z"
        stroke="#201C3E"
        stroke-linecap="round"
        stroke-linejoin="round"
      />
      <path
        d="M59.346 46.985a.974.974 0 0 1-.69-.932v-3h-3.5v2.972a1 1 0 0 1-.712.956 8.42 8.42 0 0 0-.9.329l2.374 6.49a.5.5 0 0 0 .934 0l2.494-6.815Z"
        fill="#fff"
        stroke="#201C3E"
        stroke-width="1.009"
        stroke-linecap="round"
        stroke-linejoin="round"
      />
      <path
        d="M50.166 69.496c.346-.008.683-.112.972-.3l-.225-4.174v-15.94a8.43 8.43 0 0 0-2.488 5.99v12.687a1.742 1.742 0 0 0 1.741 1.737ZM61.858 48.236v19.526a1.74 1.74 0 1 0 3.482 0V55.075a8.445 8.445 0 0 0-3.482-6.839ZM56.385 31.618a5.97 5.97 0 0 1 5.97 5.97v.8a1.218 1.218 0 0 1-.125 2.41 5.97 5.97 0 0 1-11.815-1.22v-1.99a5.97 5.97 0 0 1 5.97-5.97Z"
        fill="#fff"
        stroke="#201C3E"
        stroke-linecap="round"
        stroke-linejoin="round"
      />
      <path
        d="M56.385 38.428a.746.746 0 1 0 1.492 0 .746.746 0 0 0-1.492 0ZM52.405 38.428a.746.746 0 1 0 1.492 0 .746.746 0 0 0-1.492 0Z"
        fill="#201C3E"
      />
      <path d="M58.375 40.421a.746.746 0 1 0 1.492 0 .746.746 0 0 0-1.492 0Z" fill="#8F5AFF" />
      <path
        d="M56.883 42.157a2.69 2.69 0 0 1-2.985 0"
        stroke="#201C3E"
        stroke-linecap="round"
        stroke-linejoin="round"
      />
      <path
        d="M62.786 33.087c.6 1.031 1.454 1.52 1.454 6s1.492 4.477 1.492 7.462c0 2.692-2.247 3.361-5.016 3.466a.5.5 0 0 1-.46-.729c.674-1.274 1.86-4.3 1.983-9.928a.9.9 0 0 0-.836-.915c-2.54-.178-7.342-2.673-8.046-3.93a.594.594 0 0 0-.753-.256c-1.53.66-2.19 3.337-2.19 3.337v2.1c-.004 3.131.49 6.243 1.466 9.218a.415.415 0 0 1-.613.49c-3.664-2.272-4.142-3.188-2.947-6.835.514-1.569-.995-8.955 1.99-9.95 0-2.488 11.674-1.392 12.476.47Z"
        fill="#fff"
        stroke="#201C3E"
        stroke-linecap="round"
        stroke-linejoin="round"
      />
      <path
        d="M57.988 33.322c-3.068-1.306-6.616-1.583-7.678-.7a6.97 6.97 0 0 1 .11-2.4c.783-1.84 4.815-1.886 9.007-.1 4.192 1.786 6.3 3.99 6.8 5.98.5 1.99-.867 2.32-1.99 2.488-.025-1.381-3.125-3.94-6.249-5.268Z"
        fill="#8F5AFF"
        stroke="#201C3E"
        stroke-linecap="round"
        stroke-linejoin="round"
      />
      <path
        d="M50.324.954a2.528 2.528 0 0 1 2.528 2.528v21.2a2.528 2.528 0 0 1-2.528 2.528h-2.742v3.787a1.05 1.05 0 0 1-1.793.742l-4.527-4.527H37.6l-2.7 6.937a1.05 1.05 0 0 1-1.958 0l-2.7-6.937h-3.65l-4.527 4.527a1.05 1.05 0 0 1-1.793-.742v-3.784h-2.724a2.528 2.528 0 0 1-2.528-2.528V3.482A2.528 2.528 0 0 1 17.548.954h32.776Z"
        fill="#fff"
        stroke="#201C3E"
        stroke-linecap="round"
        stroke-linejoin="round"
      />
      <path
        d="M39.385 12.526a5.457 5.457 0 1 0-9.633 3.513 4.85 4.85 0 0 1 1.148 3.125v1.246h6.064v-1.246a4.84 4.84 0 0 1 1.136-3.125 5.43 5.43 0 0 0 1.285-3.513Z"
        fill="#fff"
      />
      <path
        d="M39.385 12.526a5.457 5.457 0 1 0-9.633 3.513 4.85 4.85 0 0 1 1.148 3.125v1.246h6.064v-1.246a4.84 4.84 0 0 1 1.136-3.125 5.43 5.43 0 0 0 1.285-3.513ZM35.746 22.839h-3.638a1.213 1.213 0 0 1-1.208-1.213v-1.213h6.064v1.213a1.212 1.212 0 0 1-1.218 1.213Z"
        fill="#8F5AFF"
        stroke="#201C3E"
        stroke-linejoin="round"
      />
      <path
        d="M35.746 23.444a1.819 1.819 0 1 1-3.638 0v-.606h3.638v.606Z"
        fill="#8F5AFF"
        stroke="#201C3E"
        stroke-linejoin="round"
      />
      <path
        d="M33.927 4.649V3.436M39.502 6.956l.857-.858M41.811 12.527h1.213M28.352 6.956l-.858-.858M26.043 12.527H24.83"
        stroke="#201C3E"
        stroke-linecap="round"
        stroke-linejoin="round"
      />
    </svg>
  );
}
