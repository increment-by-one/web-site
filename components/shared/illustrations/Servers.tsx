import { ClassName } from '@/lib/types';
import classNames from 'classnames';

export default function Servers({ className }: { className?: ClassName }) {
  return (
    <svg
      className={classNames(className)}
      style={{ fill: 'none' }}
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 80 54"
    >
      <path
        d="M35.038 42.208a2.368 2.368 0 0 1-2.36 2.361H3.167a2.367 2.367 0 0 1-2.36-2.36V22.141h34.23v20.066Z"
        fill="#8F5AFF"
        stroke="#201C3E"
        stroke-linecap="round"
        stroke-linejoin="round"
      />
      <path
        d="M35.038 22.144H.808v-4.721a2.368 2.368 0 0 1 2.36-2.361h29.51a2.37 2.37 0 0 1 2.36 2.36v4.722Z"
        fill="#fff"
        stroke="#201C3E"
        stroke-linecap="round"
        stroke-linejoin="round"
      />
      <path
        d="M3.169 18.602a1.18 1.18 0 1 0 2.36 0 1.18 1.18 0 0 0-2.36 0ZM7.89 18.602a1.18 1.18 0 1 0 2.36 0 1.18 1.18 0 0 0-2.36 0ZM12.61 18.602a1.18 1.18 0 1 0 2.36 0 1.18 1.18 0 0 0-2.36 0ZM57.323 18.602a1.18 1.18 0 1 0 2.36 0 1.18 1.18 0 0 0-2.36 0ZM79.192 42.208a2.367 2.367 0 0 1-2.36 2.361h-29.51a2.368 2.368 0 0 1-2.36-2.36V22.141h34.23v20.066Z"
        fill="#8F5AFF"
        stroke="#201C3E"
        stroke-linecap="round"
        stroke-linejoin="round"
      />
      <path
        d="M79.192 22.144h-34.23v-4.721a2.369 2.369 0 0 1 2.36-2.361h29.51a2.368 2.368 0 0 1 2.36 2.36v4.722Z"
        fill="#fff"
        stroke="#201C3E"
        stroke-linecap="round"
        stroke-linejoin="round"
      />
      <path
        d="M52.045 18.602a1.18 1.18 0 1 0 2.36 0 1.18 1.18 0 0 0-2.36 0ZM56.766 18.602a1.18 1.18 0 1 0 2.36 0 1.18 1.18 0 0 0-2.36 0Z"
        fill="#8F5AFF"
        stroke="#201C3E"
        stroke-linecap="round"
        stroke-linejoin="round"
      />
      <path
        d="M73.39 29.814a1.967 1.967 0 0 1-1.968 1.967H52.733a1.968 1.968 0 0 1 0-3.935h18.69a1.968 1.968 0 0 1 1.966 1.968Z"
        fill="#fff"
        stroke="#201C3E"
        stroke-linecap="round"
        stroke-linejoin="round"
      />
      <path
        d="M54.263 9.274a2.48 2.48 0 0 1-2.48 2.48H28.217a2.48 2.48 0 1 1 0-4.96h23.564a2.48 2.48 0 0 1 2.481 2.48Z"
        fill="#fff"
      />
      <path d="M44.961 6.801H28.218a2.48 2.48 0 1 0 0 4.96h16.743v-4.96Z" fill="#8F5AFF" />
      <path
        d="M61.58 34.9a2.985 2.985 0 0 1-2.98 2.98H21.4a2.985 2.985 0 0 1-2.98-2.98V9.6h43.16v25.3Z"
        fill="#8F5AFF"
        stroke="#201C3E"
        stroke-linecap="round"
        stroke-linejoin="round"
      />
      <path
        d="M61.58 9.599H18.42V3.648A2.985 2.985 0 0 1 21.4.672h37.2a2.985 2.985 0 0 1 2.98 2.976v5.95Z"
        fill="#fff"
        stroke="#201C3E"
        stroke-linecap="round"
        stroke-linejoin="round"
      />
      <path
        d="M21.397 5.139a1.488 1.488 0 1 0 2.976 0 1.488 1.488 0 0 0-2.976 0ZM27.35 5.139a1.488 1.488 0 1 0 2.975 0 1.488 1.488 0 0 0-2.975 0ZM33.303 5.139a1.488 1.488 0 1 0 2.975 0 1.488 1.488 0 0 0-2.975 0Z"
        fill="#8F5AFF"
        stroke="#201C3E"
        stroke-linecap="round"
        stroke-linejoin="round"
      />
      <path
        d="M54.263 19.274a2.48 2.48 0 0 1-2.481 2.48H28.218a2.48 2.48 0 0 1 0-4.96h23.564a2.48 2.48 0 0 1 2.48 2.48Z"
        fill="#8F5AFF"
      />
      <path
        d="M54.263 19.274a2.48 2.48 0 0 1-2.481 2.48H28.218a2.48 2.48 0 0 1 0-4.96h23.564a2.48 2.48 0 0 1 2.48 2.48Z"
        fill="url(#a)"
      />
      <path
        d="M54.263 19.274a2.48 2.48 0 0 1-2.481 2.48H28.218a2.48 2.48 0 0 1 0-4.96h23.564a2.48 2.48 0 0 1 2.48 2.48Z"
        stroke="#201C3E"
        stroke-linecap="round"
        stroke-linejoin="round"
      />
      <path
        d="M24.616 18.77a2.308 2.308 0 0 1 2.307-2.308h26.154a2.308 2.308 0 0 1 2.308 2.307v32.308a2.308 2.308 0 0 1-2.308 2.308H26.923a2.308 2.308 0 0 1-2.307-2.308V18.769Z"
        fill="#8F5AFF"
      />
      <path
        d="M27.845 48.811h6.946M27.845 21.059h6.946"
        stroke="#201C3E"
        stroke-linecap="round"
        stroke-linejoin="round"
      />
      <path
        d="M49.84 21.061a1.735 1.735 0 1 0 3.47 0 1.735 1.735 0 0 0-3.47 0ZM44.053 21.061a1.735 1.735 0 1 0 3.47 0 1.735 1.735 0 0 0-3.47 0ZM49.84 48.813a1.735 1.735 0 1 0 3.47 0 1.735 1.735 0 0 0-3.47 0ZM44.053 48.813a1.735 1.735 0 1 0 3.47 0 1.735 1.735 0 0 0-3.47 0Z"
        fill="#fff"
        stroke="#201C3E"
        stroke-linecap="round"
        stroke-linejoin="round"
      />
      <path
        d="M53.313 53.442H26.687a2.316 2.316 0 0 1-2.315-2.31V44.18h31.256v6.95a2.316 2.316 0 0 1-2.315 2.31v0ZM27.845 30.32h6.946"
        stroke="#201C3E"
        stroke-linecap="round"
        stroke-linejoin="round"
      />
      <path
        d="M49.84 30.322a1.735 1.735 0 0 0 2.964 1.227 1.737 1.737 0 1 0-2.964-1.227ZM44.053 30.322a1.735 1.735 0 0 0 2.963 1.227 1.737 1.737 0 1 0-2.963-1.227Z"
        fill="#fff"
        stroke="#201C3E"
        stroke-linecap="round"
        stroke-linejoin="round"
      />
      <path
        d="M24.372 25.69h31.256v9.26H24.372v-9.26ZM27.845 39.55h6.946"
        stroke="#201C3E"
        stroke-linecap="round"
        stroke-linejoin="round"
      />
      <path
        d="M49.84 39.553a1.735 1.735 0 0 0 2.964 1.227 1.738 1.738 0 0 0-1.228-2.963 1.735 1.735 0 0 0-1.736 1.736ZM44.053 39.553a1.735 1.735 0 0 0 2.963 1.227 1.738 1.738 0 0 0-1.227-2.963 1.735 1.735 0 0 0-1.736 1.736Z"
        fill="#fff"
        stroke="#201C3E"
        stroke-linecap="round"
        stroke-linejoin="round"
      />
      <path
        d="M24.372 34.92h31.256v9.261H24.372v-9.26ZM55.628 25.69H24.372v-6.947a2.316 2.316 0 0 1 2.315-2.314h26.626a2.316 2.316 0 0 1 2.315 2.315v6.945Z"
        stroke="#201C3E"
        stroke-linecap="round"
        stroke-linejoin="round"
      />
      <path
        d="M5.521 38.718a12.402 12.402 0 1 0 24.804 0 12.402 12.402 0 0 0-24.804 0Z"
        fill="#fff"
      />
      <path
        d="M5.521 38.718a12.402 12.402 0 1 0 24.804 0 12.402 12.402 0 0 0-24.804 0v0Z"
        stroke="#201C3E"
        stroke-linecap="round"
        stroke-linejoin="round"
      />
      <path
        d="M16.164 44.617a2.433 2.433 0 0 1-1.722-.713L11.2 40.656a2.435 2.435 0 0 1 3.444-3.443l1.525 1.525 5.043-5.214a2.435 2.435 0 0 1 3.438 3.443l-6.764 6.937a2.429 2.429 0 0 1-1.722.713Z"
        fill="#8F5AFF"
      />
      <path
        d="M16.164 44.617a2.433 2.433 0 0 1-1.722-.713L11.2 40.656a2.435 2.435 0 0 1 3.444-3.443l1.525 1.525 5.043-5.214a2.435 2.435 0 0 1 3.438 3.443l-6.764 6.937a2.429 2.429 0 0 1-1.722.713Z"
        fill="url(#b)"
      />
      <path
        d="M16.164 44.617a2.433 2.433 0 0 1-1.722-.713L11.2 40.656a2.435 2.435 0 0 1 3.444-3.443l1.525 1.525 5.043-5.214a2.435 2.435 0 0 1 3.438 3.443l-6.764 6.937a2.429 2.429 0 0 1-1.722.713Z"
        stroke="#201C3E"
        stroke-linecap="round"
        stroke-linejoin="round"
      />
    </svg>
  );
}
