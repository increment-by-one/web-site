import { ClassName } from '@/lib/types';
import classNames from 'classnames';
import { SVGProps } from 'react';

export type FastEmailProps = SVGProps<SVGSVGElement> & {
  className?: ClassName;
};

export default function FastEmail({ className, ...props }: FastEmailProps) {
  return (
    <svg
      viewBox="0 0 170 128"
      fill="none"
      style={{ fill: 'none' }}
      xmlns="http://www.w3.org/2000/svg"
      className={classNames(className)}
      {...props}
    >
      <path
        d="M158.103 43.6062L152.474 9.13077C152.359 8.54844 152.116 7.99899 151.762 7.5224C151.409 7.0458 150.953 6.65406 150.429 6.37567C149.905 6.09728 149.325 5.93924 148.732 5.91307C148.139 5.88689 147.548 5.99322 147.001 6.22434L93.8155 28.7062C92.97 29.0634 92.2735 29.701 91.843 30.5117C91.4126 31.3223 91.2746 32.2565 91.4522 33.157L97.0812 67.6323C97.1958 68.2147 97.4388 68.7641 97.7925 69.2407C98.1462 69.7173 98.6018 70.109 99.1259 70.3874C99.6501 70.6658 100.23 70.8239 100.823 70.85C101.416 70.8762 102.007 70.7699 102.554 70.5388L155.733 48.0637C156.581 47.7074 157.28 47.0693 157.712 46.2571C158.143 45.4449 158.282 44.5085 158.103 43.6062Z"
        fill="white"
      />
      <path
        d="M158.104 43.6062C158.281 44.5067 158.143 45.4408 157.713 46.2515C157.282 47.0621 156.586 47.6997 155.74 48.0569L102.561 70.532C102.014 70.7631 101.423 70.8694 100.83 70.8433C100.237 70.8171 99.6577 70.659 99.1335 70.3807C98.6094 70.1023 98.1539 69.7105 97.8002 69.2339C97.4464 68.7573 97.2034 68.2079 97.0888 67.6255L91.4598 33.157C91.2817 32.2573 91.4187 31.3238 91.8479 30.5132C92.277 29.7027 92.9721 29.0646 93.8163 28.7062L146.995 6.23117C147.542 6.00006 148.133 5.89372 148.726 5.9199C149.319 5.94608 149.899 6.10411 150.423 6.3825C150.947 6.66089 151.403 7.05264 151.756 7.52923C152.11 8.00582 152.353 8.55527 152.468 9.13761L158.104 43.6062Z"
        stroke="#3E2A78"
        strokeWidth="1.70266"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M120.325 38.567C121.817 38.9291 123.381 38.8536 124.83 38.3496C126.28 37.8456 127.553 36.9344 128.498 35.7253L149.579 6.04966C148.724 5.82266 147.818 5.88614 147.003 6.23014L93.8167 28.712C93.0625 29.0302 92.4246 29.573 91.9897 30.2665L120.325 38.567Z"
        fill="white"
        stroke="#3E2A78"
        strokeWidth="1.70266"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M48.8848 72.2257L83.0043 57.8008"
        stroke="#3E2A78"
        strokeWidth="1.70266"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M47.1807 83.7872L83.6073 68.3867"
        stroke="#3E2A78"
        strokeWidth="1.70266"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M38.9775 87.2547L42.657 85.7002"
        stroke="#3E2A78"
        strokeWidth="1.70266"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M33.4609 89.5874L34.3804 89.1992"
        stroke="#3E2A78"
        strokeWidth="1.70266"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M35.0891 78.0581L36.0085 77.6699"
        stroke="#3E2A78"
        strokeWidth="1.70266"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M38.5603 65.7544L39.4797 65.3662"
        stroke="#3E2A78"
        strokeWidth="1.70266"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M40.6091 75.7264L44.2869 74.1719"
        stroke="#3E2A78"
        strokeWidth="1.70266"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M52.354 59.9192L82.0432 47.3672"
        stroke="#3E2A78"
        strokeWidth="1.70266"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M44.0767 63.4217L47.7561 61.8672"
        stroke="#3E2A78"
        strokeWidth="1.70266"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M114.757 36.9336L99.1348 70.3908"
        stroke="#3E2A78"
        strokeWidth="1.70266"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M131.683 31.2383L158.13 44.994"
        stroke="#3E2A78"
        strokeWidth="1.70266"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M78.091 35.164L75.2067 17.5024C75.1485 17.2039 75.0243 16.9221 74.8433 16.6777C74.6623 16.4333 74.429 16.2324 74.1604 16.0897C73.8919 15.9469 73.5949 15.8659 73.291 15.8526C72.9872 15.8392 72.6842 15.8939 72.4042 16.0125L45.1617 27.531C44.7283 27.7137 44.3713 28.0402 44.1507 28.4555C43.9302 28.8709 43.8597 29.3496 43.9511 29.8109L46.8235 47.4742C46.8819 47.7728 47.0062 48.0545 47.1874 48.2989C47.3686 48.5433 47.6021 48.7441 47.8709 48.8867C48.1396 49.0293 48.4368 49.1101 48.7408 49.1232C49.0447 49.1363 49.3477 49.0813 49.6277 48.9623L76.8702 37.4456C77.3058 37.2646 77.6653 36.9386 77.8878 36.5227C78.1104 36.1068 78.1822 35.6268 78.091 35.164Z"
        fill="#8F5AFF"
      />
      <path
        d="M78.0908 35.164C78.1826 35.6256 78.1123 36.1046 77.8917 36.5203C77.6712 36.936 77.3138 37.2629 76.8802 37.4456L49.6377 48.9623C49.3572 49.0832 49.0531 49.1397 48.7479 49.1275C48.4427 49.1154 48.1441 49.035 47.8741 48.8922C47.6041 48.7494 47.3695 48.5479 47.1877 48.3025C47.0058 48.0571 46.8812 47.7741 46.8232 47.4742L43.9474 29.8109C43.856 29.3496 43.9265 28.8709 44.1471 28.4555C44.3676 28.0402 44.7247 27.7137 45.158 27.531L72.4005 16.0125C72.6805 15.8939 72.9835 15.8392 73.2874 15.8526C73.5912 15.8659 73.8882 15.9469 74.1568 16.0897C74.4253 16.2324 74.6586 16.4333 74.8396 16.6777C75.0206 16.9221 75.1448 17.2039 75.2031 17.5024L78.0908 35.164Z"
        stroke="#3E2A78"
        strokeWidth="1.70266"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M58.7407 32.5798C59.5049 32.7654 60.3063 32.7267 61.049 32.4682C61.7916 32.2096 62.4439 31.7423 62.9276 31.1223L73.7241 15.9193C73.2862 15.8028 72.822 15.8351 72.4045 16.0112L45.162 27.5297C44.7755 27.6929 44.4485 27.9711 44.2256 28.3266L58.7407 32.5798Z"
        stroke="#3E2A78"
        strokeWidth="1.70266"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M55.8826 31.7441L47.8784 48.8848"
        stroke="#3E2A78"
        strokeWidth="1.70266"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M64.5547 28.8271L78.1044 35.8744"
        stroke="#3E2A78"
        strokeWidth="1.70266"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M12.0862 57.9726L12.8711 57.6406"
        stroke="#3E2A78"
        strokeWidth="1.70266"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M15.0442 47.4775L15.8274 47.1455"
        stroke="#3E2A78"
        strokeWidth="1.70266"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M23.8467 52.9994L40.2501 46.0645"
        stroke="#3E2A78"
        strokeWidth="1.70266"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M16.7903 55.9868L19.9283 54.6621"
        stroke="#3E2A78"
        strokeWidth="1.70266"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M26.8069 42.5079L38.5688 37.5361"
        stroke="#3E2A78"
        strokeWidth="1.70266"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M19.7485 45.4924L22.8848 44.166"
        stroke="#3E2A78"
        strokeWidth="1.70266"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M117.335 96.2702L114.451 78.6085C114.393 78.3098 114.269 78.0278 114.088 77.7833C113.906 77.5387 113.673 77.3377 113.404 77.1949C113.135 77.0521 112.838 76.9712 112.534 76.958C112.23 76.9448 111.927 76.9997 111.647 77.1187L84.4043 88.6371C83.971 88.8198 83.614 89.1463 83.3934 89.5617C83.1729 89.977 83.1024 90.4557 83.1937 90.917L86.0746 108.58C86.1339 108.878 86.2589 109.159 86.4405 109.403C86.622 109.646 86.8556 109.846 87.1243 109.988C87.3929 110.13 87.6898 110.21 87.9933 110.223C88.2968 110.236 88.5994 110.181 88.8789 110.062L116.121 98.5449C116.554 98.363 116.911 98.0375 117.133 97.6232C117.354 97.2089 117.425 96.7311 117.335 96.2702Z"
        fill="#8F5AFF"
      />
      <path
        d="M117.336 96.2701C117.427 96.7317 117.357 97.2108 117.137 97.6265C116.916 98.0422 116.559 98.369 116.125 98.5517L88.8791 110.062C88.5992 110.181 88.2961 110.236 87.9922 110.223C87.6882 110.209 87.391 110.129 87.1223 109.986C86.8536 109.843 86.6201 109.643 86.4389 109.398C86.2576 109.154 86.1333 108.872 86.0749 108.574L83.1906 90.917C83.0992 90.4557 83.1697 89.977 83.3902 89.5617C83.6108 89.1463 83.9678 88.8198 84.4012 88.6371L111.644 77.1187C111.924 76.9997 112.227 76.9448 112.531 76.958C112.835 76.9712 113.132 77.0521 113.401 77.1949C113.67 77.3377 113.903 77.5387 114.084 77.7833C114.266 78.0278 114.39 78.3098 114.448 78.6085L117.336 96.2701Z"
        stroke="#3E2A78"
        strokeWidth="1.70266"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M97.9802 93.6853C98.7432 93.8697 99.5431 93.8305 100.284 93.5723C101.026 93.3142 101.677 92.848 102.16 92.2295L112.96 77.0248C112.522 76.9082 112.058 76.9406 111.641 77.1167L84.3981 88.6352C84.0122 88.7986 83.6859 89.0768 83.4634 89.432L97.9802 93.6853Z"
        stroke="#3E2A78"
        strokeWidth="1.70266"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M95.1255 92.8496L87.1213 109.99"
        stroke="#3E2A78"
        strokeWidth="1.70266"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M103.799 89.9336L117.349 96.9809"
        stroke="#3E2A78"
        strokeWidth="1.70266"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M51.3306 119.079L52.1138 118.747"
        stroke="#3E2A78"
        strokeWidth="1.70266"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M54.2871 108.59L55.072 108.258"
        stroke="#3E2A78"
        strokeWidth="1.70266"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M63.0928 114.113L79.4962 107.178"
        stroke="#3E2A78"
        strokeWidth="1.70266"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M56.0347 117.095L59.171 115.769"
        stroke="#3E2A78"
        strokeWidth="1.70266"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M66.0493 103.614L77.8113 98.6426"
        stroke="#3E2A78"
        strokeWidth="1.70266"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M58.991 106.599L62.1273 105.272"
        stroke="#3E2A78"
        strokeWidth="1.70266"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  );
}
