import Image, { ImageProps } from 'next/image';
import { AllowedImageElement, BorderedImageProps } from './BorderedImage.types';
import styles from './BorderedImage.module.css';

export default function BorderedImage({
  alt,
  height,
  layout,
  src,
  width,
  as = AllowedImageElement.IMG,
}: BorderedImageProps) {
  let imageElement;

  if (as === AllowedImageElement.NEXT_IMAGE) {
    const imageProps: ImageProps = { alt, layout, src };

    if (width || height) {
      imageProps.height = height;
      imageProps.width = width;
    }

    imageElement = <Image {...{ ...imageProps, alt }} />;
  } else {
    // eslint-disable-next-line @next/next/no-img-element
    imageElement = <img src={src} alt={alt} />;
  }

  return <figure className={styles.BorderedImage}>{imageElement}</figure>;
}
