export enum AllowedImageElement {
  NEXT_IMAGE = 'next_image',
  IMG = 'img',
}

export type BorderedImageProps = {
  alt: string;
  src: string;

  as?: AllowedImageElement;
  height?: string | number;
  layout?: 'fill' | 'fixed' | 'intrinsic' | 'responsive';
  width?: string | number;
};
