import styles from './CompetencyList.module.css';
import { PropsWithChildren } from 'react';

export default function Competency({ title, children }: PropsWithChildren<{ title: string }>) {
  return (
    <li className={styles.Competency}>
      <strong className={styles.Competency__title}>{title}</strong>
      {children}
    </li>
  );
}
