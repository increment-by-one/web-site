import styles from './CompetencyList.module.css';
import { PropsWithChildren } from 'react';

export default function CompetencyList({
  children,
}: PropsWithChildren<{ title?: string; description?: string }>) {
  return (
    <ul className={styles.CompetencyList} data-role="list">
      {children}
    </ul>
  );
}
