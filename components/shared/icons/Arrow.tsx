import classNames from 'classnames';
import { IconProps } from '.';

export default function Arrow({ className }: IconProps) {
  return (
    <svg
      className={classNames(className)}
      style={{ fill: 'none' }}
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 18 18"
    >
      <path
        fill="#ECEAF5"
        fillRule="evenodd"
        d="M8.293 1.192a1 1 0 0 1 1.414 0l7.071 7.071a1 1 0 0 1 0 1.415l-7.071 7.07a1 1 0 0 1-1.414-1.414l5.364-5.364H1.929a1 1 0 1 1 0-2h11.728L8.293 2.606a1 1 0 0 1 0-1.414Z"
        clipRule="evenodd"
      />
    </svg>
  );
}
