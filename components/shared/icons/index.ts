import { ClassName } from '@/lib/types';

export type IconProps = {
  className?: ClassName;
};
