import classNames from 'classnames';
import { IconProps } from '.';

export default function Burger({ className }: IconProps) {
  return (
    <svg
      className={classNames(className)}
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 24 24"
    >
      <path
        d="M2.25 18.003h19.5M2.25 12.003h19.5M2.25 6.003h19.5"
        stroke="currentColor"
        strokeWidth="1.5"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  );
}
