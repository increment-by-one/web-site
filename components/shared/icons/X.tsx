import classNames from 'classnames';
import { IconProps } from '.';

export default function X({ className }: IconProps) {
  return (
    <svg
      className={classNames(className)}
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 24 24"
    >
      <path
        d="m5.105 19 13.79-13.789M5.105 5l13.79 13.789"
        stroke="currentColor"
        strokeWidth="1.5"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  );
}
