import CaseStudy from '@/components/content/CaseStudy';
import RelatedCaseStudies from '@/components/content/RelatedCaseStudies';
import Footer from '@/components/layout/Footer';
import Hero from '@/components/layout/Hero';
import PageSection, { PageSectionType } from '@/components/layout/PageSection';
import NextProject from '@/components/shared/NextProject';
import { getTags } from '@/lib/openGraph';
import {
  FULL_CASE_STUDY_FIELDS,
  getCaseStudies,
  getCaseStudy,
  getCaseStudySlugs,
  serializePost,
  SHORT_CASE_STUDY_FIELDS,
} from '@/lib/posts';
import { Post } from '@/lib/types';
import { GetStaticProps, NextPage } from 'next';
import Head from 'next/head';
import { ParsedUrlQuery } from 'querystring';

interface PostParsedUrlQuery extends ParsedUrlQuery {
  slug: string;
}

type CaseStudyPostPageProps = {
  caseStudy: Post;
  otherProjects: Post[];
};

const CaseStudyPost: NextPage<CaseStudyPostPageProps> = ({ caseStudy, otherProjects }) => {
  return !caseStudy ? null : (
    <>
      <Head>
        <title>{caseStudy.openGraph?.title || caseStudy.title}</title>

        {getTags({
          description: caseStudy.openGraph?.description || caseStudy.summary,
          image: `/images/open-graph/case-study-${caseStudy.slug}.png`,
          title: caseStudy.openGraph?.title || caseStudy.title,
          path: caseStudy.permalink,
        })}
      </Head>
      <>
        <Hero />

        <main>
          <CaseStudy {...caseStudy} />

          <PageSection title="Other Projects" variant={PageSectionType.HIGHLIGHT}>
            <RelatedCaseStudies posts={otherProjects} />
          </PageSection>

          <PageSection variant={PageSectionType.INVERTED_DARK}>
            <NextProject />
          </PageSection>
        </main>

        <Footer />
      </>
    </>
  );
};

export async function getStaticPaths() {
  const postPaths = await getCaseStudySlugs();
  const posts = await Promise.all(
    postPaths.map((path) => getCaseStudy(path, SHORT_CASE_STUDY_FIELDS))
  );
  const paths: Array<{ params: { slug: string } }> = [];
  posts.forEach(({ publishedAt, slug }) => {
    if (publishedAt) {
      paths.push({ params: { slug } });
    }
  });

  return {
    paths,
    fallback: true,
  };
}

export const getStaticProps: GetStaticProps = async ({ params }) => {
  const { slug } = params as PostParsedUrlQuery;

  // Load the content for the post
  const [caseStudy, latestCaseStudies] = await Promise.all([
    getCaseStudy(slug, FULL_CASE_STUDY_FIELDS),
    getCaseStudies(SHORT_CASE_STUDY_FIELDS, 4),
  ]);

  serializePost(caseStudy);

  const otherCaseStudies: Post[] = [];
  latestCaseStudies.forEach((caseStudy) => {
    if (caseStudy.slug !== slug) {
      serializePost(caseStudy);
      otherCaseStudies.push(caseStudy);
    }
  });

  return {
    props: {
      caseStudy,
      otherProjects: otherCaseStudies.slice(0, 3),
    },
    revalidate: 3600,
  };
};

export default CaseStudyPost;
