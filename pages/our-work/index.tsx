import NextProject from '@/components/shared/NextProject';
import CaseStudyGrid from '@/components/content/CaseStudyGrid';
import Footer from '@/components/layout/Footer';
import Hero, { HeroVariant } from '@/components/layout/Hero';
import PageSection, { PageSectionType } from '@/components/layout/PageSection';
import { getTags } from '@/lib/openGraph';
import { getCaseStudies, serializePost, SHORT_CASE_STUDY_FIELDS } from '@/lib/posts';
import { Post } from '@/lib/types';
import { GetStaticProps, NextPage } from 'next';
import Head from 'next/head';

type CaseStudyIndexPageProps = {
  caseStudies: Post[];
};

const CaseStudyIndex: NextPage<CaseStudyIndexPageProps> = ({ caseStudies = [] }) => {
  return (
    <>
      <Head>
        <title>Our Work | Increment By One</title>

        {getTags({
          title: 'Our Work | Increment By One',
          description: `If you're looking for a development team, you'll want evidence of their capabilities. Take a look at our case studies to see our projects in the wild.`,
          path: '/our-work',
        })}
      </Head>
      <>
        <Hero variant={HeroVariant.INVERTED} title="See our projects in the wild" />

        <main>
          <PageSection
            variant={PageSectionType.SUMMARY}
            title="Making technical development as easy as Pie"
          >
            <p>
              Want to see evidence of our work? Our case studies showcase our projects, detail our
              process and discuss the results - so if you need added reassurance about what we do,
              take a look at some of them below or get in touch directly to ask any questions you
              may have.
            </p>
          </PageSection>

          <PageSection>
            <CaseStudyGrid posts={caseStudies} />
          </PageSection>

          <PageSection variant={PageSectionType.INVERTED_DARK}>
            <NextProject />
          </PageSection>
        </main>

        <Footer />
      </>
    </>
  );
};

export const getStaticProps: GetStaticProps = async () => {
  // Get a page of paths and a pagination object
  const caseStudies = await getCaseStudies(SHORT_CASE_STUDY_FIELDS);

  caseStudies.forEach(serializePost);

  return {
    props: { caseStudies },
    revalidate: 3600,
  };
};

export default CaseStudyIndex;
