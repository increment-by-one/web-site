import Footer from '@/components/layout/Footer';
import Hero from '@/components/layout/Hero';
import Head from 'next/head';
import styles from '../styles/about.module.css';
import DevelopmentProcessSection from '@/components/sections/DevelopmentProcess';
import MeetTheTeamSection from '@/components/sections/MeetTheTeam';
import EcologiSection from '@/components/sections/Ecologi';
import WhyTheNameSection from '@/components/sections/WhyTheName';
import { getTags } from '@/lib/openGraph';
import { Background as HeroBackground, Inner as HeroInner } from '@/components/content/AboutHero';
import Link from 'next/link';

const About = ({ treeCount = 1300 }) => {
  return (
    <>
      <Head>
        <title>About Us | Increment By One</title>

        {getTags({
          title: 'About Us | Increment By One',
          description:
            'Software development with a difference. We put empathy into the core of our process. Meet the team here at Increment By One and learn what makes us unique.',
          path: '/about',
        })}
      </Head>
      <>
        <Hero badge="About Us" title="Putting empathy back into software development">
          <HeroInner>
            <p>
              Like any website, we&apos;ve got pages that tell you all about{' '}
              <Link href="/our-services/web-application-development">
                <a aria-label="Our Web Application Development services.">what</a>
              </Link>
              <span aria-hidden="true">.</span>
              <Link href="/our-services/server-side-development">
                <a aria-label="Our Server Side Development services.">we</a>
              </Link>
              <span aria-hidden="true">.</span>
              <Link href="/our-services/development-consultancy">
                <a aria-label="Our Web Development Consultancy services.">do</a>
              </Link>
              . But if you&apos;re on this page, specifically, then you&apos;re probably looking for
              a bit more about who we actually are. What makes us tick? Are we capable of taking on
              your project? Are we faceless robots who can accomplish complex dev tasks in the blink
              of an eye?
            </p>
            <p>
              Okay, maybe not. What we are is a small team of experienced software engineers who
              specialise in web applications and complex technical backend requirements. Founded by
              Jon Park, we&apos;ve got the tech know-how you need… but a refreshingly human-centric
              approach.
            </p>
          </HeroInner>

          <HeroBackground />
        </Hero>

        <main className={styles.About}>
          <WhyTheNameSection />

          <DevelopmentProcessSection />

          <MeetTheTeamSection />

          <EcologiSection trees={treeCount} />
        </main>

        <Footer />
      </>
    </>
  );
};

export async function getStaticProps() {
  const response = await fetch(`https://public.ecologi.com/users/incrementbyone/trees`);
  let treeCount = 1000;

  if (response.ok) {
    const body = await response.json();

    treeCount = Math.floor(body.total / 100) * 100;
  }

  return {
    props: { treeCount: treeCount },
    // revalidate: 3600,
  };
}

export default About;
