import PostGrid from '@/components/content/PostGrid';
import CaseStudies from '@/components/home/CaseStudies';
import ClientLogos from '@/components/home/ClientLogos';
import Services from '@/components/home/Services';
import Footer from '@/components/layout/Footer';
import Hero, { HeroSize, HeroVariant, HomeInner } from '@/components/layout/Hero';
import PageSection, {
  Header as PageSectionHeader,
  PageSectionHeaderSize,
  PageSectionTitleStyle,
  PageSectionType,
  TitleLevel,
} from '@/components/layout/PageSection';
import TechnicalCompetencies from '@/components/sections/TechnicalCompetencies';
import { getTags } from '@/lib/openGraph';
import {
  SHORT_CASE_STUDY_FIELDS,
  SHORT_POST_FIELDS,
  getCaseStudies,
  getLatestBlogPosts,
  serializePost,
} from '@/lib/posts';
import { Post } from '@/lib/types';
import type { NextPage } from 'next';
import Head from 'next/head';

type HomePageProps = {
  caseStudies: Post[];
  latestPosts: Post[];
};

const Home: NextPage<HomePageProps> = ({ caseStudies = [], latestPosts = [] }) => {
  return (
    <>
      <Head>
        <title>Outsourced Development Team Driven By Empathy | Increment By One</title>
        {getTags({
          title: 'Outsourced Development Team Driven By Empathy | Increment By One',
          description:
            'Looking for a full stack development team to build web applications from scratch or take existing projects to the next level? See what makes Increment By One different.',
          path: '/',
        })}
      </Head>
      <>
        <Hero size={HeroSize.LARGE} variant={HeroVariant.INVERTED_SQUARES}>
          <HomeInner />
        </Hero>

        <main>
          <TechnicalCompetencies />

          <PageSection
            variant={PageSectionType.HIGHLIGHT}
            titleLevel={TitleLevel.H2}
            title="Our Services"
          >
            <Services />
          </PageSection>

          <PageSection
            variant={latestPosts.length > 0 ? PageSectionType.DEFAULT : PageSectionType.HIGHLIGHT}
          >
            <PageSectionHeader
              size={PageSectionHeaderSize.LARGE}
              titleLevel={TitleLevel.H2}
              titleStyle={PageSectionTitleStyle.GRADIENT}
            >
              <strong>Helping companies big and small</strong>
              Get To the Next Level
            </PageSectionHeader>
            <ClientLogos />

            {caseStudies.length > 0 && <CaseStudies {...{ caseStudies }} />}
          </PageSection>

          {latestPosts.length > 0 && (
            <PageSection
              titleLevel={TitleLevel.H2}
              variant={PageSectionType.HIGHLIGHT}
              title="Latest News"
            >
              <PostGrid posts={latestPosts} />
            </PageSection>
          )}
        </main>

        <Footer />
      </>
    </>
  );
};

export async function getStaticProps() {
  const [caseStudies, latestPosts] = await Promise.all([
    getCaseStudies(SHORT_CASE_STUDY_FIELDS, 3),
    getLatestBlogPosts(SHORT_POST_FIELDS, 3),
  ]);

  caseStudies.forEach(serializePost);
  latestPosts.forEach(serializePost);

  return {
    props: { latestPosts, caseStudies },
    // revalidate: 3600,
  };
}

export default Home;
