import { Html, Head, Main, NextScript } from 'next/document';

export default function Document() {
  return (
    <Html lang="en-GB">
      <Head>
        <style
          dangerouslySetInnerHTML={{
            __html: `
            @font-face {
              font-family: KumbhSans;
              src: url(/fonts/KumbhSans-VariableFont_wght.ttf) format('truetype');
              font-display: swap;
            }

            :root {
              --font-family: 'KumbhSans', sans-serif;
            }

            body {
              font-family: var(--font-family);
            }
          `,
          }}
        />
      </Head>
      <body>
        <Main />
        <NextScript />
      </body>
    </Html>
  );
}
