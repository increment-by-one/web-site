import Footer from '@/components/layout/Footer';
import Hero, { Inner as HeroInner } from '@/components/layout/Hero';
import Head from 'next/head';
import Devices from '@/components/shared/illustrations/Devices';
import ServiceIntro from '@/components/sections/service/ServiceIntro';
import ServiceList from '@/components/sections/service/ServiceList';
import ServiceOutro from '@/components/sections/service/ServiceOutro';
import ListItem from '@/components/sections/service/ServiceList/ListItem';
import { getTags } from '@/lib/openGraph';

const WebApplicationDevelopment = () => {
  return (
    <>
      <Head>
        <title>Web Application Development | Increment By One</title>
        {getTags({
          title: 'Web Application Development | Increment By One',
          description: `Build better web applications with Increment By One's help. A full stack development team with a different approach to your next project. Learn more here.`,
          path: '/our-services/web-application-development',
        })}
      </Head>
      <>
        <Hero />
        <main>
          <ServiceIntro
            illustration={<Devices />}
            title="Web App Development"
            subTitle="We build better digital products that work for everyone"
          >
            <>
              <p>
                Web applications are the most versatile form of digital product, offering users a
                fast, intuitive way to access your technology from any device. By freeing your
                customers from a need to download a product or use a specific system, you&apos;ll be
                granting widespread access to your app whilst making it as versatile and
                customisable as it needs to be.
              </p>
              <p>
                At Increment By One, we specialise in web applications built to the highest
                technical standards. As specialists in front and backend development, we&apos;ll
                ensure your product looks and feels great for users, but also conforms to the
                technical demands of your data and industry.
              </p>
              <p>
                Whether you&apos;re looking to launch a new SaaS product or develop a
                device-agnostic product for your existing business, we&apos;ll work with you to
                understand your needs and then increment on them to deliver an app that exceeds your
                expectations.
              </p>
            </>
          </ServiceIntro>
          <ServiceList
            title="The benefits of web app development"
            description="Web app development has a number of clear benefits in a fast-moving digital age:"
          >
            <ListItem title="Cross-platform accessibility:">
              There&apos;s never been a more important time to take accessibility into account. From
              the actual physical design of your system through to how it&apos;s accessed, you need
              to make sure as many users as possible can use it without issues. Web apps give you
              more flexibility and freedom to design across platforms and devices, so no one is
              limited by what they own.
            </ListItem>
            <ListItem title="Scalable and versatile:">
              What begins as a small web app can grow into a huge success - just as Zoom. We&apos;ll
              build your web app to meet your existing needs, but also offer a pathway to iteration
              and updates that keeps it at the forefront of your customer&apos;s minds.
            </ListItem>
            <ListItem title="Security through consolidation:">
              With multiple apps and products comes more complexity. A single web app means you have
              greater control over your database and product with a single, securely backed-up,
              solution.
            </ListItem>
          </ServiceList>
          <ServiceOutro
            title="SaaS App Development Experts"
            url="/contact-us"
            linkText="Get In Touch"
          >
            <p>
              Software as a service was worth over 150 billion USD in 2021, with no signs of slowing
              down. If you&apos;re planning to launch a SaaS system, choose Increment By One. We
              build apps that grow as your business does, driving updates as a result of customer
              behaviours, business changes and technical requirements. That means you&apos;ll get an
              SaaS product that will evolve as it gains customers, staying ahead of competition and
              delivering commercial growth.
            </p>
          </ServiceOutro>
        </main>
        <Footer />
      </>
    </>
  );
};

export default WebApplicationDevelopment;
