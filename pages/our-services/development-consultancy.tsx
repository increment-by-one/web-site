import Footer from '@/components/layout/Footer';
import Hero, { Inner as HeroInner } from '@/components/layout/Hero';
import Head from 'next/head';
import Consultancy from '@/components/shared/illustrations/Consultancy';
import ServiceIntro from '@/components/sections/service/ServiceIntro';
import ServiceList from '@/components/sections/service/ServiceList';
import ServiceOutro from '@/components/sections/service/ServiceOutro';
import ListItem from '@/components/sections/service/ServiceList/ListItem';
import { getTags } from '@/lib/openGraph';

const DevelopmentConsultancy = () => {
  return (
    <>
      <Head>
        <title>Software Development Consultancy | Increment By One</title>
        {getTags({
          title: 'Software Development Consultancy | Increment By One',
          description:
            'Looking for development consultants who can build from scratch or rescue a failing project? Increment By One will help you with a whole new approach to software development.',
          path: '/our-services/development-consultancy',
        })}
      </Head>
      <>
        <Hero />
        <main>
          <ServiceIntro
            illustration={<Consultancy />}
            title="Software Development Consultancy"
            subTitle="Build software just as individual and unique as your business."
          >
            <>
              <p>
                If off-the-shelf doesn&apos;t cut it, where do you turn? Searching for software
                development teams to help you build something unique means identifying someone that
                has the technical competency you need, but also the vision to help you map out the
                project as a whole.
              </p>
              <p>
                Just as your own business has its unique personalities, processes, quirks and
                systems, all software has its own advantages, disadvantages, charms and challenges.
                As a software development team with a rich background in both back and frontend
                development, we can help you build a project that&apos;s entirely unique to your
                requirements and your business&apos; specific goals.
              </p>
              <p>
                That means taking the time to understand the ins and outs of your needs. We
                won&apos;t write a single line of code until we&apos;ve got a clear picture of what
                you want, who your end-users are and what they need. We&apos;ll also assess the
                technical side of the project to assign the right system for your company – you may
                or may not need a complex JavaScript web application, so why pay more than you need
                to?
              </p>
              <p>
                If you&apos;d like to work with us on a consultancy level, we&apos;ll integrate with
                your existing team or act as your remote development resource, helping guide you
                through the entire software process to deliver commercial results.
              </p>
            </>
          </ServiceIntro>
          <ServiceList
            title="Why bespoke development?"
            description="While our web application and server side development services are listed individually due to customer demand, we believe that ultimately, the best projects are those where a developer can combine the technical and user-facing parts of a program to build something that works for your end-users. To do that, you need bespoke development consultancy, which gives you:"
          >
            <ListItem title="Customised to user needs:">
              we don&apos;t build based solely on your initial expectations. We&apos;ll gather a
              full understanding of your business and its users, then suggest technical products
              that solve user challenges rather than simply &apos;ticking a box&apos;.
            </ListItem>
            <ListItem title="Suits your specific business:">
              like we said earlier, why pay more for less? If you don&apos;t need X amount of custom
              features, we&apos;ll tell you. You&apos;ll get a product designed for your business
              and employees, not something overly complex meant to show off our skills.
            </ListItem>
            <ListItem title="Fully scalable:">
              Many of our clients come to us with nitty-gritty technical issues that they need
              support with. We&apos;ll perform a consultancy role for a while, but it&apos;s not
              long before we are asked to take on larger projects due to the knowledge and
              capabilities we demonstrate. Just look at our case studies to learn more.
            </ListItem>
          </ServiceList>
          <ServiceOutro
            title="SaaS from the ground up"
            url="/contact-us"
            linkText="See Web App Development"
          >
            <p>
              Software as a service is a flourishing industry and offers a promising sales model for
              existing businesses or start-ups. To build your own software, maintain the system and
              ensure the platform is kept updated to customer expectations, you need a software team
              that understands the front and backend of development and the nature of cloud-based
              web applications. Increment By One are specialists in SaaS – click below to learn
              more.
            </p>
          </ServiceOutro>
        </main>
        <Footer />
      </>
    </>
  );
};

export default DevelopmentConsultancy;
