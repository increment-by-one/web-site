import Footer from '@/components/layout/Footer';
import Hero, { HeroVariant, Inner as HeroInner } from '@/components/layout/Hero';
import ServiceOverview from '@/components/sections/ServiceOverview';
import Button, { ButtonSize } from '@/components/shared/Button';
import { getTags } from '@/lib/openGraph';
import PageSection, { PageSectionType } from '@/components/layout/PageSection';
import Head from 'next/head';
import styles from '../../styles/services.module.css';
import NextProject from '@/components/shared/NextProject';

const OurServices = () => {
  return (
    <>
      <Head>
        <title>Services | Full Stack Development | Increment By One</title>

        {getTags({
          title: 'Services | Full Stack Development | Increment By One',
          description:
            'Whether you want to build a web application from scratch or rescue a project, Increment By One has the technical expertise and skills needed to help. Discover our full range of services here.',
          path: '/our-services',
        })}
      </Head>
      <>
        <Hero variant={HeroVariant.INVERTED} title="What We Do">
          <HeroInner>
            <p>A brief overview on how IBO can help your project</p>
          </HeroInner>
        </Hero>

        <main>
          <div className={styles.OurServices__background}>
            <div className={styles.OurServices__intro}>
              <div className={styles.OurServices__inner}>
                <h2 className={styles.OurServices__title}>
                  Don&apos;t compromise on your development team.
                </h2>
                <p className={styles.OurServices__subtitle}>
                  Combine technical expertise with user-focused flair
                </p>
                <div className={styles.OurServices__copy}>
                  <p>
                    Choosing a full stack development agency can often feel like a slog. It can feel
                    like every website is just shouting about how many coding languages they work
                    in, how much project experience they have and what their client base thinks.
                  </p>
                  <p>
                    But really, these things should all be a given. A great development team is
                    competent with any modern code base and will have experience in a wide variety
                    of sectors.
                  </p>
                  <p>
                    And in those things, we&apos;re no different. What makes Increment By One unique
                    is that we don&apos;t take your requirements at face value. We won&apos;t just
                    accept a brief and start coding in the dark. Instead, you&apos;ll come to us
                    with your idea or problem and we&apos;ll work to understand every aspect of it
                    before we begin assigning a solution.
                  </p>
                  <p>
                    We turn your business requirements into digital solutions - then improve them
                    even further. To do that, we offer distinct services that can be supplied
                    individually or as a whole project. Take a look at them below, or get in touch
                    now to discuss your needs
                  </p>
                </div>

                <Button
                  size={ButtonSize.LARGE}
                  href="/contact-us"
                  className={styles.Navigation__link}
                >
                  Get In Touch
                </Button>
              </div>
            </div>
          </div>

          <ServiceOverview service="web-application-development" />

          <ServiceOverview service="server-side-development" />

          <ServiceOverview service="development-consultancy" />

          <PageSection variant={PageSectionType.INVERTED_DARK}>
            <NextProject />
          </PageSection>
        </main>

        <Footer />
      </>
    </>
  );
};

export default OurServices;
