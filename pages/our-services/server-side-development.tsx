import Footer from '@/components/layout/Footer';
import Hero, { Inner as HeroInner } from '@/components/layout/Hero';
import Head from 'next/head';
import Servers from '@/components/shared/illustrations/Servers';
import ServiceIntro from '@/components/sections/service/ServiceIntro';
import ServiceList from '@/components/sections/service/ServiceList';
import ServiceOutro from '@/components/sections/service/ServiceOutro';
import ListItem from '@/components/sections/service/ServiceList/ListItem';
import { getTags } from '@/lib/openGraph';

const ServiceSideDevelopment = () => {
  return (
    <>
      <Head>
        <title>Server-side Development | Increment By One</title>
        {getTags({
          title: 'Server-side Development | Increment By One',
          description:
            'Upgrade your existing servers and backend programs or create new ones with Increment By One - our team brings technical expertise and empathy to your project.',
          path: '/our-services/server-side-development',
        })}
      </Head>
      <>
        <Hero />
        <main>
          <ServiceIntro
            illustration={<Servers />}
            title="Server Side Development"
            subTitle="Build reliable systems that your team can depend upon."
          >
            <>
              <p>
                The &apos;backend&apos; side of development is a complex thing - with the technical
                architecture and terminology involved often leading to confusion, poor workflows and
                a reliance on outdated processes. How many of us have worked somewhere that still
                uses old, out of date systems because they can&apos;t update the backend?
              </p>
              <p>
                At Increment By One, we help demystify the server side development process.
                We&apos;ll work with you to build a complete understanding of your requirements and
                expectations, then build the system you need in a way that respects the idea of
                growth and forward progress. That means we won&apos;t tie you into working
                exclusively with our team, instead providing development work that leaves things
                better than they were before - so other future developers can work more effectively
                if you want to update or enhance parts of your product.
              </p>
              <p>
                With a deep understanding of all technical coding languages, frameworks and
                technologies, our team can build robust, secure server systems and applications to
                power your business. With our front-end expertise, we&apos;ll also make sure any
                work is done with user experience in mind. Why sacrifice function or form? Choose
                both with Increment By One.
              </p>
            </>
          </ServiceIntro>
          <ServiceList
            title="Our server side development expertise"
            description="When you're approaching a team to work on a technical part of your business, you need confidence that they have the skills, experience and understanding required to manage the work in a cost-effective manner. We achieve this through a number of factors: "
          >
            <ListItem title="Technical competency:">
              we&apos;ve worked in virtually every coding language there is - specialising in the
              mainstays of our sector. Whatever requirements you have, you&apos;re likely to find
              solutions directly within Increment By One&apos;s team.
            </ListItem>
            <ListItem title="Empathy-driven:">
              we&apos;re not here to sell you anything. You need support, so we provide it – which
              means digging into the details of your requirements and trying to add value with our
              approach. If we don&apos;t see that we can provide value to your project, we&apos;ll
              tell you.
            </ListItem>
            <ListItem title="Iterative mentality:">
              while we&apos;re working on your project, we don&apos;t settle for the standard
              delivery model. Instead, we&apos;ll try to add value and improve the overall project
              through our own expertise and insight. Once finished, we don&apos;t rest on our
              laurels - we&apos;ll always be willing to actively update and iterate as required.
            </ListItem>
          </ServiceList>
          <ServiceOutro
            title="Want the total package?"
            url="/contact-us"
            linkText="See web app development"
          >
            <p>
              Web applications provide both a front and backend element, ideal for new projects that
              need a digital product launched to a wide audience with as minimal dev time as
              possible. By removing the need for multiple product &apos;versions&apos; across
              devices, you can launch a working model that can start generating revenue as soon as
              possible. Choose Increment By One and we&apos;ll bring our technical, server side
              expertise as well as our front-end skills to your web app project.
            </p>
          </ServiceOutro>
        </main>
        <Footer />
      </>
    </>
  );
};

export default ServiceSideDevelopment;
