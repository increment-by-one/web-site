import Footer from '@/components/layout/Footer';
import Hero, { Inner as HeroInner } from '@/components/layout/Hero';
import Header from '@/components/layout/Header';
import Head from 'next/head';
import styles from '../styles/thank-you.module.css';
import Button, { ButtonLevel, ButtonSize } from '@/components/shared/Button';
import SparkIllustration from '@/components/shared/illustrations/Spark';
import { getTags } from '@/lib/openGraph';

const Contact = () => {
  return (
    <>
      <Head>
        <title>Thank you for getting in touch! | Increment By One</title>

        {getTags({
          title: 'Thank you for getting in touch! | Increment By One',
          path: '/thank-you',
        })}
      </Head>
      <>
        <Hero />

        <main className={styles.ThankYou}>
          <SparkIllustration className={styles.ThankYou__illustration} />
          <header>
            <h1 className={styles.ThankYou__title}>Thanks!</h1>
            <p className={styles.ThankYou__subtitle}>We’ll be in touch, usually within 48 hours</p>
          </header>
          <div className={styles.ThankYou__copy}>
            <p>
              In the meantime why not take a look at some of our recent work, or read our recent
              posts on the blog.
            </p>
          </div>
          <footer className={styles.ThankYou__cta}>
            <Button href="/our-work" level={ButtonLevel.SECONDARY} size={ButtonSize.MEDIUM}>
              View Work
            </Button>
            <Button href="/news" level={ButtonLevel.SECONDARY} size={ButtonSize.MEDIUM}>
              Read Recent Articles
            </Button>
          </footer>
        </main>
        <Footer />
      </>
    </>
  );
};

export default Contact;
