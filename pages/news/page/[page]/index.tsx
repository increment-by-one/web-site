import PostGrid from '@/components/content/PostGrid';
import Footer from '@/components/layout/Footer';
import Hero from '@/components/layout/Hero';
import PageSection from '@/components/layout/PageSection';
import { getTags } from '@/lib/openGraph';
import { getBlogPathsForPage, getBlogPosts, serializePost, SHORT_POST_FIELDS } from '@/lib/posts';
import { Pagination, Post } from '@/lib/types';
import { GetStaticProps, NextPage } from 'next';
import Head from 'next/head';
import { ParsedUrlQuery } from 'querystring';

interface NewsPagePageParsedUrlQuery extends ParsedUrlQuery {
  page: string;
}

type NewsPagePageProps = {
  page: string;
  posts: Post[];
  pagination: Pagination;
};

const NewsPage: NextPage<NewsPagePageProps> = ({ page, posts = [], pagination }) => {
  return !page ? null : (
    <>
      <Head>
        <title>News | Increment By One</title>

        {getTags({
          title: 'News | Increment By One',
          description: 'Sharing our knowledge as we continue to learn, and news as it happens.',
          path: `/news/page/${page}`,
        })}
      </Head>
      <>
        <Hero title="Industry News & Insights" badge={`Page ${pagination.page}`} />

        <main>
          <PageSection>
            <PostGrid posts={posts} />
          </PageSection>
        </main>

        <Footer />
      </>
    </>
  );
};

export async function getStaticPaths() {
  const {
    pagination: { totalPages },
  } = await getBlogPathsForPage(1, 9);

  const paths = [];
  for (let i = 1; i <= totalPages; i += 1) {
    paths.push({ params: { page: `${i}` } });
  }

  return {
    paths,
    fallback: true,
  };
}

export const getStaticProps: GetStaticProps = async ({ params }) => {
  const { page } = params as NewsPagePageParsedUrlQuery;

  // Get a page of paths and a pagination object
  const { paths, pagination } = await getBlogPathsForPage(parseInt(page, 10), 9);

  // Load the content for the posts
  const posts = await getBlogPosts(paths, SHORT_POST_FIELDS);

  posts.forEach(serializePost);

  return {
    props: { page, posts, pagination },
    revalidate: 3600,
  };
};

export default NewsPage;
