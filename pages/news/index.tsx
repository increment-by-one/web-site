import PostGrid from '@/components/content/PostGrid';
import Footer from '@/components/layout/Footer';
import Hero from '@/components/layout/Hero';
import PageSection from '@/components/layout/PageSection';
import { getTags } from '@/lib/openGraph';
import { getBlogPathsForPage, getBlogPosts, serializePost, SHORT_POST_FIELDS } from '@/lib/posts';
import generateRssFeed from '@/lib/rss';
import { Pagination, Post } from '@/lib/types';
import { NextPage } from 'next';
import Head from 'next/head';

type NewsIndexPageProps = {
  posts: Post[];
  pagination?: Pagination;
};

const NewsIndex: NextPage<NewsIndexPageProps> = ({ posts = [], pagination }) => {
  return (
    <>
      <Head>
        <title>News | Increment By One</title>

        {getTags({
          title: 'News | Increment By One',
          description: 'Sharing our knowledge as we continue to learn, and news as it happens.',
          path: '/news',
        })}
      </Head>
      <div className="page page--light page--outlined">
        <Hero title="Industry News & Insights" badge="Latest News" />

        <main>
          <PageSection>
            <PostGrid posts={posts} />
          </PageSection>
        </main>

        <Footer />
      </div>
    </>
  );
};

export async function getStaticProps() {
  // Get a page of paths and a pagination object
  const [{ paths, pagination }] = await Promise.all([getBlogPathsForPage(1, 9), generateRssFeed()]);

  // Load the content for the posts
  const posts = await getBlogPosts(paths, SHORT_POST_FIELDS);

  posts.forEach(serializePost);

  return {
    props: { posts, pagination },
    revalidate: 3600,
  };
}

export default NewsIndex;
