import Footer from '@/components/layout/Footer';
import Hero, { HeroVariant, Inner as HeroInner } from '@/components/layout/Hero';
import {
  FULL_POST_FIELDS,
  getBlogPost,
  getBlogPostPaths,
  getLatestBlogPosts,
  serializePost,
  SHORT_POST_FIELDS,
} from '@/lib/posts';
import { Post as PostType, PostAuthor } from '@/lib/types';
import { GetStaticProps, NextPage } from 'next';
import Head from 'next/head';
import { ParsedUrlQuery } from 'querystring';
import { PostItemProps } from '@/components/content/PostItem/PostItem.types';
import Author from '@/components/content/Author';
import format from 'date-fns/format';
import Post from '@/components/content/Post';
import RelatedPosts from '@/components/content/RelatedPosts';
import { getTags } from '@/lib/openGraph';
import PageSection from '@/components/layout/PageSection';

interface NewsPostParsedUrlQuery extends ParsedUrlQuery {
  year: string;
  month: string;
  slug: string;
}

type NewsPostPageProps = {
  post: {
    image: string;
    slug: string;
    summary: string;
    title: string;
    publishedAt: string;
    updatedAt?: string;
    author: PostAuthor | null;
    content: string;
  };
  latestPosts: PostType[];
};

const NewsPost: NextPage<NewsPostPageProps> = ({ slug, post, relatedPosts }: PostItemProps) => {
  if (!post) {
    console.log('NO POST FOUND FOR SLUG: ', slug);
    return null;
  }
  const publishedAtTime = new Date(post.publishedAt);

  return (
    <>
      <Head>
        <title>{`${post.title} - News | Increment By One`}</title>

        {getTags({
          title: post.openGraph?.title || post.title,
          description: post.openGraph?.description || post.summary,
          path: post.permalink,
        })}
      </Head>
      <>
        <Hero
          variant={HeroVariant.INVERTED}
          title={post.title}
          background={post.images.cover || '/images/backgrounds/blog-general.jpg'}
        >
          <HeroInner>
            <Author {...post.author} isInverted>
              <time dateTime={publishedAtTime.toISOString()}>
                {format(publishedAtTime, 'MMM do, yyyy')}
              </time>
            </Author>
          </HeroInner>
        </Hero>

        <main>
          <PageSection>
            <Post {...post} />
          </PageSection>
          <RelatedPosts posts={relatedPosts} />
        </main>

        <Footer />
      </>
    </>
  );
};

export async function getStaticPaths() {
  const paths = await getBlogPostPaths();

  return {
    paths: paths.map((path) => {
      const [year, month, slug] = path.split('/');

      return { params: { year, month, slug } };
    }),
    fallback: true,
  };
}

export const getStaticProps: GetStaticProps = async ({ params }) => {
  const { year, month, slug } = params as NewsPostParsedUrlQuery;
  const path = `${year}/${month}/${slug}`;

  // Load the content for the posts
  const [post, latestPosts] = await Promise.all([
    getBlogPost(path, FULL_POST_FIELDS),
    getLatestBlogPosts(SHORT_POST_FIELDS, 4),
  ]);

  const relatedPosts: PostType[] = [];
  latestPosts.forEach((post) => {
    if (post.slug !== slug) {
      relatedPosts.push(serializePost(post));
    }
  });
  serializePost(post);

  return {
    props: { slug, post, relatedPosts: relatedPosts.slice(0, 3) },
    revalidate: 3600,
  };
};

export default NewsPost;
