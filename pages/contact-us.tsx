import Footer from '@/components/layout/Footer';
import Hero from '@/components/layout/Hero';
import PageSection, { PageSectionType } from '@/components/layout/PageSection';
import Button from '@/components/shared/Button';
import EnvelopeIllustration from '@/components/shared/illustrations/Envelope';
import { getTags } from '@/lib/openGraph';
import classNames from 'classnames';
import Head from 'next/head';
import { useRouter } from 'next/router';
import styles from '../styles/contact-us.module.css';

const Contact = () => {
  const CONTACT_URL = process.env.NEXT_PUBLIC_CONTACT_URL || '';

  const router = useRouter();
  const hasError = router && ((router.query.error as string) || false);

  return (
    <>
      <Head>
        <title>Increment By One:: Contact Us</title>

        {getTags({
          title: 'Contact Us | Increment By One',
          description:
            'Looking to work with a software development team with a difference? Get in touch with Increment By One for a human-centred approach to technical system design.',
          path: '/contact-us',
        })}
      </Head>
      <>
        <Hero />
        <main>
          <PageSection variant={PageSectionType.SPLIT}>
            <div>
              <h1 className={styles.Contact__title}>Contact Us</h1>
              <p className={styles.Contact__subtitle}>
                Get in touch with our team in whatever way you&apos;re comfortable with.
              </p>
              <p className={styles.Contact__copy}>
                When you&apos;re ready to reach out to someone you want to work with, it can be hard
                to know the right approach. Video and phone calls are useful tools, but they&apos;re
                intimidating and isolating for some people - so we&apos;re happy to chat in whatever
                format works best for you.
              </p>
              <ul className={styles.Contact__channels}>
                <li>
                  Drop us an email:
                  <a href="mailto:hey@incrementby.one" className={styles.Contact__link}>
                    hey@incrementby.one
                  </a>
                </li>
                <li>
                  Schedule a video call:
                  <a href="https://calendly.com/jonspark/30min" className={styles.Contact__link}>
                    View Calendar
                  </a>
                </li>
              </ul>

              <form className={styles.Form} action={CONTACT_URL} method="POST">
                <fieldset className={styles.FormFieldSet}>
                  <legend className="is-visually-hidden">Send us an email</legend>

                  {hasError && (
                    <div className={styles.FormFieldSet__error}>
                      <p>{`Error: ${hasError
                        .split('-')
                        .map((word) => `${word.slice(0, 1).toUpperCase()}${word.slice(1)}`)
                        .join(' ')}`}</p>
                    </div>
                  )}

                  <div className={styles.FormFieldSet__container}>
                    <div className={styles.FormFieldSet__field} data-required>
                      <label htmlFor="contact--name" className={styles.FormField__label}>
                        Your name
                      </label>
                      <input
                        className={styles.FormField__input}
                        id="contact--name"
                        type="text"
                        name="name"
                        autoComplete="name"
                        required
                      />
                    </div>

                    <div className={classNames(styles.FormFieldSet__field)}>
                      <label htmlFor="contact--phone" className={styles.FormField__label}>
                        Phone Number
                      </label>
                      <input
                        className={styles.FormField__input}
                        id="contact--phone"
                        type="tel"
                        name="phone"
                        autoComplete="tel"
                      />
                    </div>
                  </div>
                  <div className={styles.FormFieldSet__container}>
                    <div className={styles.FormFieldSet__field} data-required>
                      <label htmlFor="contact--email" className={styles.FormField__label}>
                        Email
                      </label>
                      <input
                        className={styles.FormField__input}
                        id="contact--email"
                        type="email"
                        name="email"
                        autoComplete="email"
                        required
                      />
                    </div>

                    <div
                      className={classNames(styles.FormFieldSet__field, 'is-visually-hidden')}
                      aria-hidden
                    >
                      <label htmlFor="contact--mobile" className={styles.FormField__label}>
                        Mobile Phone Number
                      </label>
                      <input
                        className={styles.FormField__input}
                        id="contact--mobile"
                        type="text"
                        name="mobile"
                        tabIndex={-1}
                      />
                    </div>
                  </div>

                  <div className={styles.FormFieldSet__field} data-required>
                    <label className={styles.FormField__label} htmlFor="contact--message">
                      Message
                    </label>
                    <textarea
                      className={styles.FormField__input}
                      id="contact--message"
                      name="message"
                      required
                    />
                  </div>

                  <Button type="submit">Send Message</Button>
                </fieldset>
              </form>
            </div>
            <div>
              <EnvelopeIllustration className={styles.Contact__illustration} />
            </div>
          </PageSection>
        </main>
        <Footer />
      </>
    </>
  );
};

export default Contact;
