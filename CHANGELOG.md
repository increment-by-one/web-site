# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [2.4.1] - 2023-10-06

### Fixes

- Visited link styles in header.
- Canonical link double-slash.
- Teamhouse logo in Case Study Open Graph image.

## [2.4.0] - 2023-10-06

### Added

- Teamhouse case study

### Changed

- Ecologi imagery
- Homepage logo order
- Case study sort order

## [2.3.0] - 2023-06-26

### Added

- Teconnex logo
- Published new story: `native-app-vs-web-app-which-is-best-for-you`

### Changed

- Removed `role="list"` after accessibility review.
- Using `data-role="list"` as quick CSS Reset fix.
- Fix `<div>` inside `<h1>` HTML errors.
- Adds '.' to alt text on logos for screen readers.
- Fix duplicate `id` when using Logo component.

## [2.2.0] - 2023-05-26

### Added

- Haystack logo
- Raised By logo
- Durham University logo
- `meet-the-team` anchor on `/about`
- Published new story: `progressive-web-apps-are-accessibility-essentials`

## [2.1.2] - 2023-02-17

### Changed

- `digital-sustainability.md`
  - Anglicised content
  - Fixed large paragraphs in list

## [2.1.1] - 2023-02-16

### Changed

- Fixed news post ordering within months

## [2.1.0] - 2023-02-16

### Added

- Published post: Digital Sustainability

## [2.0.0] - 2023-02-06

### Changed

- Redevelopment in NextJS
- Rebrand & expanded content

## [1.1.0] - 2022-09-23

### Added

- Last deploy.

[unreleased]: https://gitlab.com/increment-by-one/internal/web-site/-/compare/2.4.1...main
[2.4.1]: https://gitlab.com/increment-by-one/internal/web-site/-/compare/2.4.0...2.4.1
[2.4.0]: https://gitlab.com/increment-by-one/internal/web-site/-/compare/2.3.0...2.4.0
[2.3.0]: https://gitlab.com/increment-by-one/internal/web-site/-/compare/2.2.0...2.3.0
[2.2.0]: https://gitlab.com/increment-by-one/internal/web-site/-/compare/2.1.2...2.2.0
[2.1.2]: https://gitlab.com/increment-by-one/internal/web-site/-/compare/2.1.0...2.1.2
[2.1.1]: https://gitlab.com/increment-by-one/internal/web-site/-/compare/2.1.0...2.1.1
[2.1.0]: https://gitlab.com/increment-by-one/internal/web-site/-/compare/1.1.0...2.1.0
[2.0.0]: https://gitlab.com/increment-by-one/internal/web-site/-/compare/1.1.0...2.0.0
[1.1.0]: https://gitlab.com/increment-by-one/internal/web-site/-/releases/tag/1.1.0
