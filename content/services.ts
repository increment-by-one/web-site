const services = {
  'web-application-development': {
    title: 'Web Application Development',
    overview:
      'Web applications bring the versatility of the internet to digital experiences – giving you the ability to get your product out to a wider audience who can interact with it via the web as opposed to downloading it to their device. We code web applications that prioritise user-experience but stay secure via a robust technical backend.',
    details: {
      title: 'These apps are:',
      items: [
        {
          title: 'An elegant, scalable option:',
          content:
            'Web applications utilise a single code base. as opposed to native products which require a new version for each device type. That means they’re more easily maintained and updated and can be scaled as you grow.',
        },
        {
          title: 'Device agnostic:',
          content:
            'There’s no real ‘user experience’ if your audience can’t access your product. Web apps prevent alienating your consumers by offering the same experience across any device, from laptops and mobiles through to desktops and tablets etc.',
        },
        {
          title: 'Integrate with your needs:',
          content:
            'Thanks to their online nature, which removes a need for a localised backend, web apps are more easily integrated with a wide variety of other platforms or software solutions.',
        },
      ],
    },
    callToAction: {
      text: 'Increment by One can build your web app from the ground-up. We’ve got vast experience in the world of SaaS and web application development, so get in touch today or click below to learn more.',
      button: 'Get web app development',
    },
  },
  'server-side-development': {
    title: 'Server side development',
    overview:
      'The technical infrastructure that sits behind every digital product needs to be robust, secure and scalable. We’ll work with you to either rescue a buggy back-end or build something unique to your business. With a team that prides itself on technical expertise and an empathy-led approach',
    details: {
      title: 'We’ll provide server side development that:',
      items: [
        {
          title: 'Keeps your data secure:',
          content:
            'In the world of online security, you can’t take any risks. Every server application or back-end project we work on will be created to the highest standards in data security and privacy.',
        },
        {
          title: 'Reduces time investment:',
          content:
            'With our streamlined approach to your technical needs, we’ll build systems that reduce the time associated with back-end processes and projects.',
        },
        {
          title: 'Improves existing systems:',
          content:
            'We’re not here to tick boxes. We’ll develop systems that improve on what came before us, and leave things better for any future developers.',
        },
      ],
    },
    callToAction: {
      text: 'Let’s take your technical needs and turn them into something more efficient. ',
      button: 'Visit server side development',
    },
  },
  'development-consultancy': {
    title: 'Development consultancy',
    overview:
      'Development is all about solving problems. A great software development team’s true value is in taking your business’ requirements into account and then coming up with technical elements that don’t just solve, but improve upon, your challenges. With that in mind, we offer a full development consultancy service that assesses your entire business requirement from beginning to end. We’ll start from scratch, build understanding and then develop the right solution, then continue post-launch with iterations and improvements based on real customer data',
    details: {
      title: 'Our process is different',
      items: [
        {
          title: 'Empathy-led:',
          content:
            'Our approach is based on empathy - for your business and for your customers. You need to know what they want to achieve and how you can help them achieve it before we write a single line of code. We’ll help you develop this understanding.',
        },
        {
          title: 'Backed by technical competency:',
          content:
            'Whatever needs our consultancy identifies, we’ve got the technical competency to accomplish them. From building new web apps through to working on existing databases, we’ve got you covered. ',
        },
        {
          title: 'Remote, but familiar:',
          content:
            'Outsourcing can be a gamble and you never know what you’re really going to get when you work with a team. Our approach invests time up-front to develop clear understanding, so you know who we are, what we’re suggesting and crucially – why we’re suggesting it.',
        },
      ],
    },
    callToAction: {
      text: 'If you’re unsure of your technical requirements but you know you need help, get in touch and we can guide you.',
      button: 'See development consultancy services',
    },
  },
};

export default services;
