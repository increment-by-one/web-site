---
title: Developing Inclusive Mobile Apps
summary: How do you develop more inclusive mobile apps? Let’s take a look at the factors limiting inclusion in mobile app design in Increment By One’s blog.
publishedAt: 2023-01-05T16:21:41
author: team-ibo
images:
  summary: /images/posts/inclusive-mobile.jpg
  cover: /images/backgrounds/inclusive-mobile-background.jpg
openGraph:
  title: Developing Inclusive Mobile Apps
  description: How do you develop more inclusive mobile apps? Let’s take a look at the factors limiting inclusion in mobile app design in Increment By One’s blog.
---

Digital inclusivity is the inclusion of people of all abilities in the digital world. This includes ensuring that digital products and services are accessible to everyone, regardless of their ability level, background or identity.

In mobile app development, inclusion is often overlooked. Some high-profile cases that have made this clear include Google’s recent attempt to launch a dermatology app that struggled to recognise people with black skin. This occurred as a result of biased sampling – when training the AI that powered the app, 90% of the database had fair skin, darker white skin or light brown skin.

This is a great example of why inclusion in mobile app design matters – it’s part of the development process rather than a problem with the product itself. As a process that has a human factor (the team creating the product, or in Google’s case above, the research team feeding the AI), app design is prone to biases, sexism, racism and other problems.

Developers must be aware of this when designing a mobile app and consider the following when planning the product:

- Is it accessible for those with disabilities and impairments? How can it be made more inclusive to those people? Apps that utilise cameras and other AR features, for example, tend to isolate people with visual impairments. Can developers account for this and design ways to make it usable?
- Will people of different races be affected differently by the product? A mobile app should present the same UX to all races, without bias.
- Will people with different gender identities experience the app in the same way? Even if an app is targeted at a specific demographic, it should still be inclusive enough to welcome other users when needed. For example, a pregnancy app aimed at women should still be welcoming to non-binary people expecting children.

## Inclusive mobile app design process

At a basic level, you’ll want to follow a simple step-by-step approach to make a more inclusive app:

1.  Conduct user research with a diverse range of users to understand their specific needs. If you’re a small team with limited access to users or resources, consider using paid user research tools like surveys and focus groups.
2.  Design the app interface and features with accessibility in mind, conforming to WCAG guidelines to ensure accessibility in every aspect of the product.
3.  Test an early app prototype with users to ensure that it is usable and accessible to all. This allows you to make changes before committing to a launch.
4.  When you do launch, start with an MVP that has the minimum level of features. Use this to gauge your inclusivity by analysing real user interaction and asking for feedback on real-world use cases.
5.  Stay on top of any technical issues and challenges a user may have, as well as keeping your app usable when platforms like Android issue mandatory updates. Make sure that the app is compatible with a range of devices and operating systems - which shows the strength of web apps.
6.  Once launched, promote the app to a wide range of users, including those with disabilities – inclusion isn’t just about the product, but also your approach and marketing efforts.

Developing inclusive mobile apps is also a matter of the technical approach. As developers, we specialise in web applications – which we also think are ideal for a more inclusive approach to product design. Not only do web apps allow users to access the same experience across multiple devices, they are also easy to iterate and pivot on – so if user needs become apparent we can quickly add features or tweak performance to be more accessible.

If you’d like to create a more inclusive web app, [contact our team](/contact-us) today. We’re here to help you understand your users in an empathetic fashion and design digital experiences that accommodate all. See our web application page for more information or get in touch now.
