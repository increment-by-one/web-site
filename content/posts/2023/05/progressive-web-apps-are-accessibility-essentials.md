---
title: Progressive Web Apps Are Accessibility Essentials
summary: What is a progressive web app and what impact will it make on user experience? Find out in this blog by full stack development agency, Increment By One.
publishedAt: 2023-05-26T11:58:01
author: team-ibo
images:
  summary: /images/posts/web-apps.jpg
  cover: /images/backgrounds/web-apps-background.jpg
openGraph:
  title: Progressive Web Apps Are Accessibility Essentials
  summary: What is a progressive web app and what impact will it make on user experience? Find out in this blog by full stack development agency, Increment By One.
---

Web applications use a single code base to deliver an app-like user experience to anyone, regardless of the device they use. As long as you can access the web, the web app should provide a usable experience – as long as the development team behind it have accommodate for certain behaviours or platform limitations.

While web apps have been a feature of the internet for many years, progressive web apps (PWA) have also risen to prominence since they were invented in 2015 as a response to the ‘multi-screen’ generation. For many users who browse the internet on smartphones as well as other devices, PWA’s may provide the ultimate in accessibility and convenience.

## What is a progressive web app?

A progressive web app blends the concept of web and mobile apps into one system. They use web technologies like a standard web app but also include many of the concepts and features used in native mobile apps such as push notifications, home screen widgets, and more. They deliver a near-native experience without the need to develop separate code bases or platform-specific quirks.

The benefits of PWA’s are clear:

- Provided you have a stable internet connection, PWA’s load very quickly. They don’t need to be installed or downloaded, so appear even faster to mobile users who are accustomed to having to download other apps.
- They are responsive and fast, delivering great user experience by allowing you to interact with no slowdowns or delays.
- PWA’s aim to deliver a natural experience on any device, meaning whether you’re browsing via your iOS or Android phone, laptop or desktop PC or even a tablet, you’ll enjoy a near native experience.
- Development costs are reduced as you won’t need to develop separate apps for iOS, Android and websites. You won’t need to pay to list on the Play or Apple Store either, so your costs are further reduced.
- PWA’s can be indexed by Google as they appear as websites in the search results – so you’ll be able to leverage SEO rankings if done correctly.

However, the cons are also evident:

- No presence on the app stores may actually harm your product’s chances to succeed as many mobile-only customers find their favourite apps exclusively through these stores.
- No PWA can offer the same performance as a native app. Ultimately, native apps will always be the higher performing on their intended devices.
- Some brands do not yet fully support PWAs. Apple, for example, has not allowed PWA developers to incorporate Siri, Bluetooth and push notifications into their apps.

## PWA’s as accessibility champions

In an age where inclusion and accessibility are key, we as software developers must do more to allow everyone to use our products. That extends beyond disabilities and identities, applying even to concepts like socio-economic group. If your product only works on high-end devices, are you limiting users who can’t afford them from access?

PWA’s help enhance accessibility by offering the following:

- Fast-loading speeds even in poor internet conditions means every user gets to enjoy a more responsive experience.
- They are device agnostic and can be used on virtually any device, meaning more people can access the app.
- They can be tested with traditional accessibility scoring systems like Google’s Lighthouse checker.
- Most PWAs have an offline version that works when a user loses connection. You’ll need to ensure both the online and offline variants pass the [WCAG 2.0](https://www.w3.org/WAI/WCAG21/quickref/) accessibility test, but assuming they do you’ll be giving all users access even if they lose internet connectivity.

The rise of PWA’s has been slower than originally predicted, but with both Google and Microsoft supporting them they seem here to stay. If you’d like to develop your own PWA and give users a seamless experience across all devices, [contact our team](/contact-us) today and we’ll help you design something inclusive, engaging and accessible.
