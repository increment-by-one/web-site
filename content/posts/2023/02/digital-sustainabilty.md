---
title: How Developers Can Lead In Digital Sustainability
summary: Digital sustainability is about minimizing the carbon footprint of digital activities and their associated real-world processes. Learn why software developers need to revolutionise digital sustainability in our blog.
publishedAt: 2023-02-16T16:35:30
author: team-ibo
images:
  summary: /images/posts/computer-developer.jpg
  cover: /images/backgrounds/computer-developer-background.jpg
openGraph:
  title: How Developers Can Lead In Digital Sustainability
  description: Digital sustainability is about minimizing the carbon footprint of digital activities and their associated real-world processes. Learn why software developers need to revolutionise digital sustainability in our blog.
---

Where the industrial revolution once transformed the world forever, so too has the digital revolution changed everything. It’s estimated that around 4.66 billion people around the world currently use the internet. While digital technology has helped bring humanity closer together and revolutionise everything from shopping to our working lives, there’s one area often overlooked: that of sustainability.

When you think of sustainability and carbon emissions, you’ll likely picture heavy industry and the automotive sector. While it’s true that these industries are the main culprits in high emissions, it’s important to recognise that the digital sector is reliant on many of these industries and also has its own impact to take into account.

## What is digital sustainability?

Digital sustainability is an important part of sustainable development. Sustainable development is development that meets the needs of the present without compromising the ability of future generations to meet their own needs. Sustainable development requires meeting the basic needs of all people, including food, water, shelter, and sanitation. It also requires reducing inequalities, protecting the environment, and ensuring that people have the opportunity to lead fulfilling lives.

Digital sustainability is a key part of sustainable development because digital technologies can help us to meet many of the goals of sustainable development. For example, digital technologies can help us to improve the efficiency of production and distribution of goods and services, to reduce waste and pollution, and to improve communication and collaboration.

Digital sustainability is therefore essential for achieving sustainable development. However, it is important to note that digital sustainability is not only about using digital technologies in a sustainable way. It is also about ensuring that the digital technologies themselves are sustainable. This means that they are designed and manufactured in a way that is environmentally sustainable, and that they are powered by renewable energy.

### Why are software developers so critical to climate change?

As software developers, we are keenly aware of the impact development can have on the environment – for both good and bad. Additionally, we must also recognise that what we create has various ‘layers’ of environmental impact:

1. The immediate impact of resources used during development: electricity, servers, the team involved, offices used, commuting etc.
2. The impact of the product itself: once the software is live, what impact does it have? Does your app cause a higher battery drain in smartphones that could lead to higher electricity usage?
3. The ‘tertiary’ impact of the product’s actual function: this is a more complex and less ‘fixable’ issue but one that showcases why it’s so important to plan out your sustainability at the beginning. By tertiary, we mean the knock-on effect of your product on user behaviour and how it can impact sustainability. Take, for example, an app like Airbnb. The mass success of Airbnb has led to a rise in tourist stays across the globe, which in turn has led to more carbon-emitting activities such as travelling to the listing, heating/cooling the property, and using data/laundry/energy more freely than they would in their own home.

These layers show us why it’s important that software developers plan environmental impact into their working lives. We are the teams who build the actual products, and it’s the products that ultimately impact sustainability.

If we recognise the carbon impact of the projects we develop, both in terms of the tangible factors like electricity usage, material cost, labour costs etc and the tertiary impacts like how it’ll change user behaviours, we can begin to develop more sustainably.

Imagine you’re faced with developing a web application that automates parts of a vehicle manufacturing plant’s build process. By recognising the biggest carbon emission factors within the process and mapping out the possible implications of your web app (if your app is good and increases output, will that raise emissions?), you can begin to plan ways to decarbonise.

In some cases, this might even mean disagreeing with your client or stepping back from a project. While it’s never nice to part ways with a client, we must all do more to promote sustainability and show that every industry is now serious about reducing our carbon footprint and creating products that help the world, not hinder it.

While much of this article has focused on carbon emissions, we should also consider the human cost of any development process. Will your product lead to jobs becoming redundant? Will the product impact physical locations in a negative way (think of something like a delivery app that may add higher density of vehicles to a city)? While it’s always up to you whether you work on the product or not, these early questions show how important a software developer’s role is in reducing carbon emissions.

As developers, we are focused on creating excellent technical solutions to our client’s problems – but we’re also aware of the potential for positive change that software can present. Here at Increment By One, we’re always trying to create more sustainable digital products and will happily work with you to map out how your product can impact the environment and humanity as a whole, then design ways to make it more sustainable and ethical.

[Get in touch](/contact-us) to get started.
