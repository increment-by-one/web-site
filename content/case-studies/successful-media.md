---
title: Successful Media
sortOrder: 3
publishedAt: 2022-01-25T09:43:56
summary: When a publishing business grew tired of their technical nightmares, we worked with them to transform their system. We took disparate technologies and created an integrated, efficient system that could be managed with ease. If you've got complex technical problems limiting your growth, read this to see how we can help.
intro:
  short: Solving Site Management Mishaps With Technology-Driven Ingenuity
  summary: For a business that based its entire revenue model on a network of lead generation websites, technical demands that hampered growth were becoming too significant to ignore. As the business looked toward growing at scale, it recognised it needed technical expertise to streamline its systems.
logo:
  height: 50
  src: /images/clients/successful-media/logo.svg
  width: 180
images:
  summary: /images/clients/successful-media/summary.jpg
  article:
    - src: /images/clients/successful-media/business-model.jpg
      alt: A person plans their business model by writing on a whiteboard.
    - src: /images/clients/successful-media/post-it-wall.jpg
      alt: Selective focus photography of yellow and orange post-it notes on wall.
    - src: /images/clients/successful-media/meeting-computer.jpg
      alt: Person talking to a meeting with computer open and phone on a table.
openGraph:
  title: Case Study Solving Site Management Mishaps | Increment By One
  description: Managing multiple websites and publishing content between them can be a challenge for any team. See how Increment By One solved this with API-driven innovations. Click to learn more.
---

### The Problem

The business in question relies on a number of lead generation/ad-revenue websites to generate income. Once it had deployed around 30 sites, the team began to realise how inefficient its system was: the lack of any centralised content management system meant that each site had to be individually updated and performance was slow across the board.

### Our Approach

We specialise in a consultancy-led approach, so we began as we always do: by having a conversation about the project and getting to know the client. The question we always ask ourselves is a simple one but it works for our client’s best interests: “**How do we make this technical challenge as easy as possible for their team?**”

During our first discussions and subsequent discovery sessions, we worked hard to build out a complete picture of the team’s requirements – but we also took the time to scope the entirety of the technical challenges they were encountering.

From this, we were able to strategise a more coherent technical approach: using static site generation powered by an API, resulting in a single point of control over any number of individual sites that could be launched at scale.

This also meant any new sites, provided the front-end design elements were already created, could be launched in record time with no technical knowledge at all.

### Results

As a result of our empathy-led initial discussions, we were able to understand the team’s problems whilst also spotting the technical failings that were limiting their growth. Our API-driven solution resulted in a number of efficiency gains including:

- A single point of input/output control in one configurable application
- The ability to manage the sites in-house with no technical expertise
- Easy content changes or new publishing with fast, responsive publishing
- Incredibly fast site development for new products, cutting the time to launch from several hours to just 17 seconds.

From struggling to balance all of the ‘spinning plates’ associated with 30+ separate sites to being able to easily manage existing sites and publish new ones at scale, Increment By One helped drive this client’s business in the right direction and give them solid foundations for future success.

Having continued to launch sites to new sectors and industries with ease, generating greater revenue than ever before, it seems clear our approach has worked. If you’ve got a nitty-gritty technical challenge hampering your own progress, get in touch with our team today.
