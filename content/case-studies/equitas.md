---
title: Equitas
sortOrder: 6
publishedAt: 2022-01-25T09:43:56
summary: When a technology start-up needed a working prototype to prove their ideas and secure investment, they struggled to find the right development team. We agreed to come on board and build their first product. This enabled the client to win further investment and expand their team. If you're a start-up looking to get your MVP out into the world as effectively as possible, read this case study to see how we can get the job done.
intro:
  short: Building Prototypes For Start-up Success
  summary: After failing to find skilled development support, an ambitious start-up team put out a cry for help in a newsletter. We answered, ready to lend technical knowledge and process-driven product development to help them establish their idea – and the results spoke for themselves.
logo:
  height: 50
  src: /images/clients/equitas/logo.svg
  width: 195
images:
  summary: /images/clients/equitas/summary.jpg
  article:
    - src: /images/clients/equitas/team-meeting.jpg
      alt: Three men sitting on chairs beside desks.
    - src: /images/clients/equitas/design-mockup.jpg
      alt: A product roadmap being discussed on a whiteboard.
    - src: /images/clients/equitas/computer-code.jpg
      alt: Person typing code on a MacBook Pro whilst having a video call.
openGraph:
  title: Case Study Building Prototypes For Start-up Success | Increment By One
  description: See how our team made a start-up brand’s idea into reality and assisted their growth into worldwide success story. Click to read now. Click to learn more.
---

### The Problem

A start-up recruitment business that had successfully entered an accelerator programme needed a working product to begin growing its business. While they had already created a rough prototype, they recognised a need for skilled development input to realise a fully working minimum viable product, or MVP.

### Our Approach

With our empathy-led approach, we were able to come on board by asking business-critical questions. We built out a full understanding of what the business wanted, why they needed it and what their users expected.

The product in question would be used in real-world interviews, so both the interviewer and interviewee's comfort, privacy and satisfaction were all core considerations which had to be assessed in light of how the product would function. We used encryption to keep data safe but also added voluntary opt-outs so that interviewees could choose not to be recorded if they wished.

We designed a platform that utilised browser video and audio calling features, which also meant dealing with cross-browser functionality issues and ensuring a smooth experience for every participant. The platform served a series of core competency questions to interviewers, who would then ask them to their interviewee – with the product recording answers and ensuring fairness across all rounds of interviews.

The product was originally designed for face-to-face interviews, but due to COVID-19 and subsequent restrictions, we were able to help the client pivot and adapt by adding online functionality to the platform.

### Results

Ultimately, our role in this project was to help a non-technical team build a complex technical product in as efficient a process as possible. We used detailed fact-finding to outline needs, built the required technical features and then pivoted to add new ones following lockdown. This responsiveness helped keep the client’s project alive during a time when many other businesses faltered. For a start-up relying on investment rounds, that was crucial.

The foundations of our product were the core requirement for the business to attract new investment. Once they had that, they could justify their idea and show real-world use cases – which led to continued investment. Using this, they could also recruit their own team and we could hand the product back to them.

This final point is important to us – we don’t hamstring clients by tying them into technical dependency. If you want us to build a product, we’ll do it in a way that allows you to take it back in-house with your own team once it’s ready and you are comfortable with it. As a technical partner, we want to be the team you can depend on, not an obligation you are dependent on.
