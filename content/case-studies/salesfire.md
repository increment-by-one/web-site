---
title: Salesfire
sortOrder: 2
summary: A quick writeup of the work you’ve complete will go here, explaining the challenges and how IBO helped to overcome these, if possible keep all of these snippets at around the same size, 4-5 lines long at the max... We can also include a small review from the client if available, as social proof.
intro:
  short: A snappy intro into this project here...
  summary: Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequaolor in reprehenderit...
logo:
  height: 50
  src: /images/clients/salesfire/logo.svg
  width: 150
images:
  summary: /images/clients/salesfire/summary.jpg
  article:
    - src: /images/clients/salesfire/placeholder-1.jpg
      alt: Lorem ipsum dolor sit amet
    - src: /images/clients/salesfire/placeholder-2.jpg
      alt: Lorem ipsum dolor sit amet
    - src: /images/clients/salesfire/placeholder-3.jpg
      alt: Lorem ipsum dolor sit amet
---

Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequaolor in reprehendLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequaolor in reprehendLorem ipsum dolor sit amet, consecteliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequaolor in reprehender.

Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequaolor in reprehenderit ...
