import { Argument } from 'classnames';

export type ClassName = string | Argument | Argument[];

export type Post = {
  [key: string]: any;
};

export type PostKey = keyof PostWithFrontMatter;

export type PostAuthor = {
  name: string;
  avatar: string;
};

export type ImageSrcWithAlt = {
  src: string;
  alt?: string;
};

export type PostImages = {
  summary: string;
  article?: ImageSrcWithAlt[];
  cover?: string;
};

export type PostIntro = {
  short: string;
  summary: string | string[];
};

export type PostWithFrontMatter = {
  title: string;
  slug: string;
  content: string;
  summary: string;
  images?: PostImages;
  publishedAt?: Date;
  updatedAt?: Date;
  openGraph: {
    title: string;
    description: string;
  };

  // ???
  permalink?: string;

  // Blog posts
  author?: 'default' | 'ted-lasso';
  excerpt?: string;

  // Case Studies
  intro?: PostIntro;
  logo?: {
    height: number;
    src: string;
    width: number;
  };
  sortOrder?: number;
};

export type Pagination = {
  page: number;
  perPage: number;
  totalPages: number;
  nextPage: number | null;
  previousPage: number | null;
  count: number;
};

export type PostSlugPageWithPagination = {
  paths: string[];
  pagination: Pagination;
};
