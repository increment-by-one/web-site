export function getTags({
  description = 'Taking you to the next level',
  image = '/images/open-graph/general.png',
  path = '/',
  title = 'Increment By One',
  url = process.env.NEXT_PUBLIC_HOST,
}: {
  description?: string;
  image?: string;
  path?: string;
  title?: string;
  url?: string;
} = {}) {
  const currentUrl = `${url}${path}`.replaceAll('//', '/');

  return [
    <link key="canonical" rel="canonical" href={currentUrl} />,
    <meta key="description" name="description" content={description} />,
    <meta key="image" name="image" content={image} />,

    <meta key="og:type" property="og:type" content="website" />,
    <meta key="og:title" property="og:title" content={title} />,
    <meta key="og:description" property="og:description" content={description} />,
    <meta key="og:url" property="og:url" content={currentUrl} />,
    <meta key="og:image" property="og:image" content={image} />,

    <meta key="twitter:card" property="twitter:card" content="summary_large_image" />,
    <meta key="twitter:title" property="twitter:title" content={title} />,
    <meta key="twitter:description" property="twitter:description" content={description} />,
    <meta key="twitter:image" property="twitter:image" content={image} />,
  ];
}
