import fs from 'fs';
import RSS, { FeedOptions } from 'rss';
import { getBlogPathsForPage, getBlogPosts, RSS_POST_FIELDS } from './posts';

export default async function generateRssFeed() {
  const siteUrl = process.env.NEXT_PUBLIC_HOST || '';

  const feed = new RSS({
    title: 'Increment By One News',
    description: 'News and updates from the team at Increment By One.',
    site_url: siteUrl,
    feed_url: `${siteUrl}/rss.xml`,
    image_url: `${siteUrl}/images/logos/rss.png`,
    language: 'en-GB',
    pubDate: new Date(),
    copyright: `All rights reserved 2006-${new Date().getFullYear()}, Increment By One Ltd.`,
  });

  const { paths } = await getBlogPathsForPage(1, 15);
  const posts = await getBlogPosts(paths, RSS_POST_FIELDS);

  posts.forEach(({ content, permalink, publishedAt, title }) => {
    feed.item({
      title,
      description: content,
      url: `${siteUrl}${permalink}`,
      date: publishedAt,
    });
  });

  fs.writeFileSync('./public/rss.xml', feed.xml({ indent: true }));
}
