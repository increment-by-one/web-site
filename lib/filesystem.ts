import fs from 'fs/promises';
import { join } from 'path';

export async function getFolders(path: string) {
  const folderPath = join(process.cwd(), 'content', path);
  const folderContents = await fs.readdir(folderPath, { withFileTypes: true });

  return folderContents.filter((item) => item.isDirectory()).map((folder) => folder.name);
}

export async function getSlugs(path: string) {
  const filePath = join(process.cwd(), 'content', path);
  const allFiles = await fs.readdir(filePath);

  const slugs: string[] = [];
  allFiles.forEach((file) => {
    if (file.slice(-3) === '.md') {
      slugs.push(file.slice(0, -3));
    }
  });

  return slugs;
}

export function getContentFile(path: string, slug: string) {
  const fullPath = join(process.cwd(), 'content', path, `${slug}.md`);

  return fs.readFile(fullPath, 'utf8');
}
