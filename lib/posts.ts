import matter from 'gray-matter';
import { remark } from 'remark';
import html from 'remark-html';
import authors from '@/content/authors.json';
import { getContentFile, getFolders, getSlugs } from './filesystem';
import {
  Pagination,
  Post,
  PostKey,
  PostSlugPageWithPagination,
  PostWithFrontMatter,
} from './types';

enum ContentPaths {
  BLOG = 'posts',
  CASE_STUDY = 'case-studies',
}

export const SHORT_CASE_STUDY_FIELDS: PostKey[] = [
  'permalink',
  'slug',
  'title',
  'logo',
  'images',
  'summary',
  'publishedAt',
];

export const FULL_CASE_STUDY_FIELDS: PostKey[] = [
  'slug',
  'title',
  'images',
  'intro',
  'summary',
  'openGraph',
  'permalink',
  'publishedAt',
  'updatedAt',
  'author',
  'content',
];

export const RSS_POST_FIELDS: PostKey[] = [
  'author',
  'content',
  'images',
  'permalink',
  'publishedAt',
  'slug',
  'summary',
  'title',
];

export const SHORT_POST_FIELDS: PostKey[] = [
  'author',
  'images',
  'permalink',
  'publishedAt',
  'slug',
  'summary',
  'title',
];

export const FULL_POST_FIELDS: PostKey[] = [
  'author',
  'content',
  'excerpt',
  'images',
  'openGraph',
  'permalink',
  'publishedAt',
  'slug',
  'summary',
  'title',
  'updatedAt',
];

export async function getCaseStudySlugs() {
  return getSlugs(ContentPaths.CASE_STUDY);
}

/**
 * Get a fixed number of Case Studies sorted by their sortOrder field
 *
 * @param fields  PostKey[] A set of fields to return from the content
 * @param count   number    The amount of case studies to fetch
 * @returns Promise<Post[]>
 */
export async function getCaseStudies(fields: PostKey[] = [], count?: number) {
  const slugs = await getCaseStudySlugs();

  const studies = await Promise.all(
    slugs.map((slug) =>
      getContentFile(ContentPaths.CASE_STUDY, slug).then((studyFile) => {
        const { data, content } = matter(studyFile);

        return { ...data, slug, content } as PostWithFrontMatter;
      })
    )
  );

  // Sort all case studies by their order
  studies.sort((a, b) => (a.sortOrder || 0) - (b.sortOrder || 0));

  // Filter out any case studies that haven't been published
  const publishedStudies = studies.filter(({ publishedAt }) => Boolean(publishedAt));
  const studyList = count ? publishedStudies.slice(0, count) : publishedStudies;

  return Promise.all(studyList.map((study) => buildPost('our-work', study, fields)));
}

export async function getCaseStudy(slug: string, fields: PostKey[]) {
  const study = await getContentFile(ContentPaths.CASE_STUDY, slug).then((studyFile) => {
    const { data, content } = matter(studyFile);

    return { ...data, slug, content } as PostWithFrontMatter;
  });

  return buildPost('our-work', study, fields);
}

/**
 * Traverses the /content/posts folder and returns an array of slugs in date order
 *
 * @returns Promise<string[]>
 */
export async function getBlogPostPaths() {
  const pathTree: { [key: string]: { [key: string]: { slug: string; published: number }[] } } = {};
  const paths: string[] = [];

  // Find all the Year folders
  const yearFolders = await getFolders('posts');

  // Find all the Month folders inside each Year folder
  await Promise.all(
    yearFolders.map((year) => {
      if (!(year in pathTree)) {
        pathTree[year] = {};
      }

      return getFolders(`${ContentPaths.BLOG}/${year}`).then((months) => {
        return months.reverse().map((month) => {
          pathTree[year][month] = [];

          return month;
        });
      });
    })
  );

  // Find all the files in a Month folder in each Year
  const years = Object.keys(pathTree);
  const monthPromises: Promise<string | void>[] = [];
  const filePromises: Promise<string | void>[] = [];

  years.forEach((year) => {
    const monthsInYear = Object.keys(pathTree[year]);

    // For every Month in every Year...
    monthsInYear.forEach((month) => {
      // ...find the slugs of the files they contain
      const promise: Promise<string | void> = getSlugs(
        `${ContentPaths.BLOG}/${year}/${month}`
      ).then((files) => {
        // For each file found...
        return files.forEach((file) => {
          // Look up the content to get the publishedAt date
          const promise = getContentFile(`${ContentPaths.BLOG}/${year}/${month}`, file).then(
            (fileContent) => {
              const {
                data: { publishedAt },
              } = matter(fileContent);

              // Track the file slug and the published date
              if (publishedAt) {
                pathTree[year][month].push({
                  slug: file,
                  published: new Date(publishedAt).getTime(),
                });
              }
            }
          );

          // Track the promise so we know when we're done
          filePromises.push(promise);

          return file;
        });
      });

      // Track the promise so we know when we're done
      monthPromises.push(promise);
    });
  });

  // Wait for all the Month look-ups
  await Promise.all(monthPromises);

  // Wait for all the File look-ups
  await Promise.all(filePromises);

  // Sort the posts by date
  years.sort((a, b) => parseInt(b, 10) - parseInt(a, 10));
  years.forEach((year) => {
    Object.keys(pathTree[year]).forEach((month) => {
      pathTree[year][month].sort((a, b) => b.published - a.published);

      pathTree[year][month].forEach(({ slug }) => {
        paths.push(`${year}/${month}/${slug}`);
      });
    });
  });

  return paths;
}

/**
 * Finds a list of slugs for a page of posts
 *
 * @param page    number  The current page number
 * @param perPage number  The number of posts per page
 * @returns Promise<PostSlugPageWithPagination>
 */
export async function getBlogPathsForPage(page: number, perPage: number) {
  const paths = await getBlogPostPaths();

  // Work out Pagination object
  const totalPages = Math.ceil(paths.length / perPage);
  const nextPage = page < totalPages ? page + 1 : null;
  const previousPage = page > 1 ? page - 1 : null;
  const count = paths.length;

  const start = page === 1 ? 0 : (page - 1) * perPage;
  const end = page * perPage;
  const pagePaths = paths.slice(start, end);

  const pagination: Pagination = { page, perPage, totalPages, nextPage, previousPage, count };

  return {
    pagination,
    paths: pagePaths,
  } as PostSlugPageWithPagination;
}

/**
 * Fetches the latest set of blog post as Post objects
 *
 * @param fields  PostKey[] A set of fields to return from the content
 * @param count   number    The maximum number of posts to return
 * @returns Promise<Post[]>
 */
export async function getLatestBlogPosts(fields: PostKey[], count: number = 3) {
  const paths = await getBlogPostPaths();

  return getBlogPosts(paths.slice(0, count), fields);
}

/**
 * Builds a Post object from a selection of fields
 *
 * @param path    string              The path to the current file
 * @param item    PostWithFrontMatter The contents of a post markdown file
 * @param fields  PostKey[]           A set of fields to return from the content
 * @returns Promise<Post>
 */
async function buildPost(path: string, item: PostWithFrontMatter, fields: PostKey[] | PostKey[]) {
  const post = {} as Post;

  await Promise.all(
    fields.map(async (field) => {
      if (typeof field === 'string') {
        switch (field) {
          case 'author':
            post.author = (item.author && authors[item.author]) || authors.default;
            break;

          case 'permalink':
            post.permalink = `/${path}/${item.slug}`;
            break;

          case 'slug':
            post.slug = item.slug;
            break;

          case 'content':
            const content = await remark().use(html).process(item.content);

            post.content = content.toString();
            break;

          default:
            post[field] = item[field];
        }
      }
    })
  );

  return post;
}

/**
 * Build a single blog post Post object from a file path
 *
 * @param path string The path to the content file
 * @param fields A set of fields to return from the content
 * @returns Promise<Post>
 */
export async function getBlogPost(path: string, fields: PostKey[]) {
  const pathParts = path.split('/');
  const slug = pathParts[pathParts.length - 1];
  const postPath = pathParts.slice(0, -1).join('/');

  return getContentFile(`${ContentPaths.BLOG}/${postPath}`, slug).then((file) => {
    const { data, content } = matter(file);

    const item = { ...data, slug, content } as PostWithFrontMatter;

    return buildPost(`news/${postPath}`, item, fields);
  }) as Promise<Post>;
}

/**
 *
 * @param paths   string[]  The paths to the blog post markdown files
 * @param fields  PostKey[] A set of fields to return from the content
 * @returns Promise<Post[]>
 */
export async function getBlogPosts(paths: string[], fields: PostKey[]) {
  return Promise.all(paths.map((path) => getBlogPost(path, fields)));
}

/**
 * Transform a Post object into something that React can serialize
 *
 * @param post Post A Post object to serialize
 * @return Post
 */
export function serializePost(post: Post) {
  const dateFields = ['publishedAt', 'updatedAt'];

  Object.keys(post).forEach((field) => {
    post[field] = post[field] || null;

    // Convert any dates into strings so they can be serialised
    if (dateFields.includes(field) && post[field] !== null) {
      post[field] = post[field].toISOString();
    }

    if (post[field] === undefined) {
      console.log({ field, value: post[field] });
    }
  });

  return post;
}
