/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,

  env: {
    NEXT_PUBLIC_CONTACT_URL: process.env.CONTACT_URL || 'http://localhost:3000/api/contact',
    NEXT_PUBLIC_HOST: process.env.APP_HOST || 'http://localhost:3000',
  }
};

module.exports = nextConfig;
